#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# MODES_LINE - Algorithme de résolution modale d'un problème mécanique linéaire
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from scipy.linalg import eig
from math import pi
from stru3d.core.assembly import assembly
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS

def MODES_LINE(model):
    """
    Résolution modale d'un problème de mécanique linéaire.
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de raideur
    M = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de masse
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage de la matrice de masse
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_MASS):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            M_elem = elem.M
            M = assembly.matrix_assembly(M, M_elem, ldof)
    
    # Sous-matrices de raideur et de massse qui correspondent aux DDL libres du système
    K11 = K[numpy.ix_(DOF['FREE'],DOF['FREE'])]
    M11 = M[numpy.ix_(DOF['FREE'],DOF['FREE'])]
    
    
    # Calcul des eigenvalues et des vecteurs propres du système matriciel
    # Le système a réssoudre est de la forme: (K-s*M)*fi = 0
    eig_val,eig_vec = eig(K11,M11)
    
    # the eigenvalues array is always of complex type
    eig_val = numpy.real(eig_val)
    
    # rangement des valeurs et des vecteurs propres du système
    idx = eig_val.argsort()
    eig_val = eig_val[idx]
    eig_vec = eig_vec[:,idx]
    
    
    # calcul des fréquences angulaires (pulsation) et fréquences propres
    ang_freq = numpy.sqrt(eig_val)
    freq = ang_freq/(2.*pi)
    
    
    # initialisation de la matrice contenant les modes propres avec valeurs
    # pour l'ensemble des noeuds du système (valeur nulle dans le cas des noeuds
    # bloqués)
    n_modes = len(freq)
    modes = numpy.zeros((n_dof,n_modes),  dtype=numpy.float)
    
    modes[DOF["FREE"],:] = eig_vec
    
    
    # normalisation des modes propres du système
    # options:
    #   MASS        - normalisation par rapport à la matrice de masse (par défaut)
    #   STIFFNESS   - normalisation par rapport à la matrice de raideur
    #   NONE        - none, les modes sont laisés intacts
    #   DISP        - le plus grand déplacement d'un noeud dans le mode considéré = 1
    #   UNIT        - déplacement d'un noeud particulier = 1 pour tous les modes
    
    for i in range(n_modes):
        
        phi = modes[:,i]
        
        if model.MODE_NORM == "MASS":
            phi = phi/dot(phi,dot(M,phi))
        
        elif model.MODE_NORM == "STIFFNESS":
            phi = phi/dot(phi,dot(K,phi))
        
        elif model.MODE_NORM == "NONE":
            pass
        
        elif model.MODE_NORM == "DISP":
            phi = phi/numpy.max(numpy.abs(phi))
        
        elif model.MODE_NORM == "UNIT":
            # not implemented yet!
            print("Error (MODES_LINE): MODE_NORM=UNIT is not implemented yet!")
            pass
        
        modes[:,i] = phi
    
    
    # facteur de participation des modes et masse modale effective
    part_fact = {}
    eff_mass  = {}
    for j,dir in enumerate(["X", "Y", "Z", "RX", "RY", "RZ"]):
        
        # vecteur delta
        idx = range(0+j,n_dof,6)
        D = numpy.zeros(n_dof, dtype=numpy.float)
        D[idx] = [1.]*len(idx)
        
        part_fact[dir] = []
        eff_mass[dir] = []
        for i in range(n_modes):
            
            phi = modes[:,i]
            
            num = dot(phi,dot(M,D))
            den = dot(phi,dot(M,phi))
            
            # facteur de participation
            ai = num/den
            part_fact[dir].append(ai)
            
            # masse modale effective
            mi = num**2/den
            eff_mass[dir].append(mi)
    
    
    # output des résultats
    model.SOLUTION["ang_freq"]  = ang_freq
    model.SOLUTION["freq"]      = freq
    model.SOLUTION["mode_no"]   = range(1,n_modes+1)
    model.SOLUTION["modes"]     = assembly.dof2node(model,modes,"disp")
    model._MODES                = modes
    model.SOLUTION["part_fact"] = part_fact
    model.SOLUTION["eff_mass"]  = eff_mass
    
    
