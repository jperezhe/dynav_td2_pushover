#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# DYNA_NON_LINE - Algorithme de résolution dynamique non-linéaire suivant le 
# schéma de résolution de Newton-Raphson et le schéma d'intégration temporelle
# de Newmark
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import array, zeros, dot
from scipy.linalg import inv        # faster than numpy
from math import sin, cos, pi, sqrt
from stru3d.core.assembly import assembly
from stru3d.core.support import interp_data
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS,CATA_ELEM_DAMP


# on définit une fonction qui vérifie la convergence d'un calcul
# non-linéaire suivant le schéma de NR
def convergence(R0,R,tol):
    """
    Retourne 
    """
    norm_R0 = sqrt(dot(R0.T,R0))
    norm_R  = sqrt(dot(R.T,R))
    if norm_R0 < 1e-30:
        return True
    else:
        if (norm_R/norm_R0) < tol:
            return True
        else:
            return False


def DYNA_NON_LINE(model):
    """
    Algorithme de résolution dynamique non-linéaire suivant le schéma
    de résolution de Newton-Raphson et le schéma d'intégration de Newmark
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    NODE_LOADS  = model.NODE_LOADS
    NODE_DISP   = model.NODE_DISP
    TIME        = model.TIME
    DT          = model.DT
    n_step = len(TIME)-1
    TOL         = model.TOL
    
    # paramètres schéma d'intégration de Newmark
    gamma_NK = 1./2.
    beta_NK  = 1./4.
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de raideur
    C = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice d'amortissement
    M = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de masse
    F = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des forces
    U = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des déplacements
    V = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des vitesses
    A = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des accélérations
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage de la matrice de masse
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_MASS):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            M_elem = elem.M
            M = assembly.matrix_assembly(M, M_elem, ldof)
    
    # Assemblage de la matrice d'amortissement
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_DAMP):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            C_elem = elem.C
            C = assembly.matrix_assembly(C, C_elem, ldof)
    
    # Calcul de la matrice d'amortissement global de Rayleigh
    if model.DAMPING:
        if model.DAMPING["type"] == "RAYLEIGH":
            w1  = 2.*pi*model.DAMPING["f1"]
            w2  = 2.*pi*model.DAMPING["f2"]
            xi1 = model.DAMPING["xi1"]
            xi2 = model.DAMPING["xi2"]
            a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
            b_ = numpy.array([xi1,xi2])
            a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
            C += a0*M + a1*K
    
    
    # Assemblage du vecteur de déplacements (déplacements imposés)
    # NOTA: pas utilisé dans cette version de l'algorithme
    
    # Assemblage du vecteur de chargement aux noeuds
    for no in NODE_LOADS.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'FX':i0, 'FY':i1, 'FZ':i2, 'MX':i3, 'MY':i4, 'MZ':i5}
        for t in NODE_LOADS[no].keys():
            F[dIndex[t]] += interp_data(NODE_LOADS[no][t]["time"],NODE_LOADS[no][t]["force"],TIME)
    
    # Force equivalente pour un chargement sismique
    if model.EARTHQUAKE:
        acc = interp_data(model.EARTHQUAKE["time"],model.EARTHQUAKE["acceleration"],TIME)
        model.SOLUTION["ACC_SUPPORT"] = acc          # sauvegarde de l'accélération appliquée au niveau du support
        dir = model.EARTHQUAKE["direction"]
        idir ={"X":0,"Y":1,"Z":2}
        delta = numpy.zeros(n_dof, dtype=numpy.float64)
        for no in DOF["NODE"].keys():
            idof = DOF["NODE"][no][idir[dir]]
            delta[idof] = 1.
        # on reactualise le vecteur de chargement avec la charge équivalente
        # (en realité F est une matrice qui contient les différents vecteurs
        # de chargement pour chaque instant du temps)
        try:
            F += -1.*numpy.outer(dot(M,delta),acc)
        
        except:
            # forces calculation of inertial forces in case of MemoryError
            d_masse = dot(M,delta)
            for i in range(0,len(TIME)):
                F[:,i] += -1.*d_masse*acc[i]
    
    # Force équivalente pour un chargement gravitaire
    if model.GRAVITY:
        acc = interp_data(model.GRAVITY["time"],model.GRAVITY["acceleration"],TIME)
        dir = model.GRAVITY["direction"]
        idir ={"X":0,"Y":1,"Z":2}
        delta = numpy.zeros(n_dof, dtype=numpy.float64)
        for no in DOF["NODE"].keys():
            idof = DOF["NODE"][no][idir[dir]]
            delta[idof] = 1.
        # on reactualise le vecteur de chargement avec la charge équivalente
        # à un chargement de poids propre pour la direction concernée
        F += numpy.outer(dot(M,delta),acc)
    
    # Décomposition de la matrice de raideur en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    K11 = K[numpy.ix_(model.DOF['FREE'],model.DOF['FREE'])]
    K12 = K[numpy.ix_(model.DOF['FREE'],model.DOF['BLOQ'])]
    K21 = numpy.transpose(K12)
    K22 = K[numpy.ix_(model.DOF['BLOQ'],model.DOF['BLOQ'])]
    
    # Décomposition de la matrice d'amortissement en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    C11 = C[numpy.ix_(model.DOF['FREE'],model.DOF['FREE'])]
    C12 = C[numpy.ix_(model.DOF['FREE'],model.DOF['BLOQ'])]
    C21 = numpy.transpose(C12)
    C22 = C[numpy.ix_(model.DOF['BLOQ'],model.DOF['BLOQ'])]
    
    # Décomposition de la matrice de masse en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    M11 = M[numpy.ix_(model.DOF['FREE'],model.DOF['FREE'])]
    M12 = M[numpy.ix_(model.DOF['FREE'],model.DOF['BLOQ'])]
    M21 = numpy.transpose(M12)
    M22 = M[numpy.ix_(model.DOF['BLOQ'],model.DOF['BLOQ'])]
    
    # Vecteur avec avec le chargement imposé aux ddl libres
    F1 = F[model.DOF['FREE']]
    
    # Vecteurs de déplacement, vitesse et accélération aux ddl libres
    U1 = U[model.DOF['FREE']]
    V1 = V[model.DOF['FREE']]
    A1 = A[model.DOF['FREE']]
    
    # vecteur des forces internes
    fSj = numpy.zeros(len(DOF['FREE']),dtype=numpy.float64)
    
    # calcul de l'accélération initiale compatible avec les conditions initiales
    # A1[:,0] = dot(inv(M11),(F1[:,0]-dot(C11,V1[:,0])-dot(K11,U1[:,0])))
    # reste à ajouter la possibilité d'insérer A1 dans A
    
    # calcul des constantes pour schéma de Newmark
    NK_a = (gamma_NK/beta_NK)*C11 + (1./(beta_NK*DT))*M11
    NK_b = DT*(gamma_NK/(2.*beta_NK)-1.)*C11 + (1./(2.*beta_NK))*M11
    
    for i in range(0,n_step):
        
        # calcul de l'incrément de force
        Dp = F1[:,i+1]-F1[:,i]
        Dpeq = Dp + dot(NK_a,V1[:,i]) + dot(NK_b,A1[:,i])
        
        # calcul de la matrice de raideur apparente
        Keq = K11 + (gamma_NK/(beta_NK*DT))*C11 + (1./(beta_NK*DT**2))*M11
        Keq_inv = inv(Keq)
        
        # Newton-Raphson modifié
        j = 0
        R = Dpeq
        U[:,i+1] = U[:,i]
        DU1 = numpy.zeros(len(DOF['FREE']),dtype=numpy.float64)
        R0 = R
        # while not convergence(R0,R,TOL) or (j == 0):
        while not (sqrt(dot(R.T,R)) < TOL) or (j == 0):
            
            # calcul de l'incrément de déplacement
            DU1j = dot(Keq_inv,R)
            
            # mise à jour du déplacement à la fin du step (attention on travaille directement
            # avec le vecteur pour tout le système pas seulement les ddl libres)
            U[DOF['FREE'],i+1] += DU1j
            
            # itération sur les éléments pour calcule le vecteur de
            # forces internes et la nouvelle matrice de raideur tangente
            fSj1_all = numpy.zeros(n_dof,dtype=numpy.float64)
            K_ = numpy.zeros((n_dof,n_dof), dtype=numpy.float64)   # matrice de raideur
            for elem in model.ELEM:
                if isinstance(elem,CATA_ELEM_STIFF):
                    ldof = numpy.concatenate([DOF['NODE'][no] for no in elem.nodes],axis=0)
                    f_int_elem,Ktan_elem = elem.iteration(U=U[ldof,i+1],time=TIME[i],dt=DT,n_iter=j)
                    fSj1_all[ldof] += f_int_elem
                    K_ = assembly.matrix_assembly(K_, Ktan_elem, ldof)
            fSj1 = fSj1_all[DOF['FREE']]
            
            # calcul de l'incrément de force réel
            Dfj = fSj1 - fSj + dot((Keq-K11),DU1j)
            
            # Mise à jour des variables avant vérification de la convergence
            R = R - Dfj
            fSj = fSj1
            DU1 += DU1j
            
            if model.SCHEME == "NR":
                # on utilise le schéma d'intégration Newton-Raphson classique
                K = K_
                K11 = K[numpy.ix_(model.DOF['FREE'],model.DOF['FREE'])]
                Keq = K11 + (gamma_NK/(beta_NK*DT))*C11 + (1./(beta_NK*DT**2))*M11
                Keq_inv = inv(Keq)
            
            if model.INFO:
                print("pas {} - iteration {} - R = {:.5e}".format(i,j,sqrt(dot(R.T,R))))
            j += 1
            
            if j > model.NITER_MAX:
                break
        
        # mise à jour du vecteur de forces du système
        # TODO
        
        # nouvelle matrice de raideur tangente à utiliser dans le prochain step
        K = K_
        K11 = K[numpy.ix_(model.DOF['FREE'],model.DOF['FREE'])]
        
        # calcul de l'incrément de vitesse et d'accélération
        DV1 = (gamma_NK/(beta_NK*DT))*DU1 - (gamma_NK/beta_NK)*V1[:,i] + \
               DT*(1.-(gamma_NK/(2.*beta_NK)))*A1[:,i]
        DA1 = (1./(beta_NK*DT**2))*DU1 - (1./(beta_NK*DT))*V1[:,i] - (1./(2.*beta_NK))*A1[:,i]
        
        # mise à jour des variables
        U1[:,i+1] = U1[:,i] + DU1
        V1[:,i+1] = V1[:,i] + DV1
        A1[:,i+1] = A1[:,i] + DA1
    
    U[DOF['FREE']] = U1
    V[DOF['FREE']] = V1
    A[DOF['FREE']] = A1
    
    # output des résultats en termes de déplacement, vitesse et accélération
    model.SOLUTION["U"] = assembly.dof2node(model,U,"disp")
    model.SOLUTION["V"] = assembly.dof2node(model,V,"disp")
    model.SOLUTION["A"] = assembly.dof2node(model,A,"disp")
    
    # output du vecteur de chargement utilisé dans le calcul
    model.SOLUTION["F"] = assembly.dof2node(model,F,"force")

