#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# DYNA_TRAN_MODAL - Algorithme de résolution de la réponse dynamique
# transitoire d'un système linéaire par superposition modale
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
import scipy
from math import pi
from stru3d.core.assembly import assembly
from stru3d.core.support import interp_data
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS,CATA_ELEM_DAMP

def DYNA_TRAN_MODAL(model):
    """
    Calcul de la réponse dynamique transitoire d'un système par
    superposition modale.
    
    IMPORTANTE: pour utiliser cet algorithme il faut que l'incrément
    de temps soit constant pour tous les pas de temps!
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    NODE_LOADS  = model.NODE_LOADS
    NODE_DISP   = model.NODE_DISP
    TIME        = model.TIME
    DT          = model.DT
    n_step = len(TIME)-1
    
    if not hasattr(model,"_MODES"):
        raise Exception("Error (DYNA_TRAN_MODAL): no MODAL analysis results are available!")
    
    
    # paramètres schéma d'intégration de Newmark
    gamma_NK = 1./2.
    beta_NK  = 1./4.
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de raideur
    C = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice d'amortissement
    M = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de masse
    F = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des forces
    U = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des déplacements
    V = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des vitesses
    A = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des accélérations
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage de la matrice de masse
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_MASS):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            M_elem = elem.M
            M = assembly.matrix_assembly(M, M_elem, ldof)
    
    # Assemblage de la matrice d'amortissement
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_DAMP):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            C_elem = elem.C
            C = assembly.matrix_assembly(C, C_elem, ldof)
    
    # Calcul de la matrice d'amortissement global de Rayleigh
    # NOTA: l'amortissement modal est traité directement lors des boucles sur les modes
    if model.DAMPING:
        if model.DAMPING["type"] == "RAYLEIGH":
            w1  = 2.*pi*model.DAMPING["f1"]
            w2  = 2.*pi*model.DAMPING["f2"]
            xi1 = model.DAMPING["xi1"]
            xi2 = model.DAMPING["xi2"]
            a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
            b_ = numpy.array([xi1,xi2])
            a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
            C += a0*M + a1*K
    
    
    # Assemblage du vecteur de chargement aux noeuds
    for no in NODE_LOADS.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'FX':i0, 'FY':i1, 'FZ':i2, 'MX':i3, 'MY':i4, 'MZ':i5}
        for t in NODE_LOADS[no].keys():
            F[dIndex[t]] += interp_data(NODE_LOADS[no][t]["time"],NODE_LOADS[no][t]["force"],TIME)
    
    # Force equivalente pour un chargement sismique
    if model.EARTHQUAKE:
        acc = interp_data(model.EARTHQUAKE["time"],model.EARTHQUAKE["acceleration"],TIME)
        dir = model.EARTHQUAKE["direction"]
        idir ={"X":0,"Y":1,"Z":2}
        delta = numpy.zeros(n_dof, dtype=numpy.float64)
        for no in DOF["NODE"].keys():
            idof = DOF["NODE"][no][idir[dir]]
            delta[idof] = 1.
        # on reactualise le vecteur de chargement avec la charge équivalente
        # (en realité F est une matrice qui contient les différents vecteurs
        # de chargement pour chaque instant du temps)
        F += -1.*numpy.outer(dot(M,delta),acc)
    
    
    # Lorsque l'utilisateur ne fournit pas une liste de modes la totalité des
    # modes sont utilisés dans le calcul
    if not model.SEL_MODES:
        n_modes = len(model.SOLUTION["freq"])
        model.SEL_MODES = range(n_modes)
    
    # Boucle sur les modes
    for j in model.SEL_MODES:
        
        # mode j
        phi = model._MODES[:,j]
        
        # calcul de la masse, amortissement, raideur et chargement généralisés pour le mode j
        mj = dot(phi,dot(M,phi))
        kj = dot(phi,dot(K,phi))
        pj = dot(phi,F)
        
        if model.DAMPING:
            if model.DAMPING["type"] == "MODAL":
                wj = model.SOLUTION["ang_freq"][j]
                xj = model.DAMPING[j]
                cj = 2.*mj*wj*xj
            else:
                cj = dot(phi,dot(C,phi))
        else:
            cj = dot(phi,dot(C,phi))
        
        
        # initialisation des vecteurs des variables dans le repère modale
        Uj = numpy.zeros(n_step+1, dtype=numpy.float64)   # vecteur des déplacements
        Vj = numpy.zeros(n_step+1, dtype=numpy.float64)   # vecteur des vitesses
        Aj = numpy.zeros(n_step+1, dtype=numpy.float64)   # vecteur des accélérations
        
        
        # ---------- résolution du système ------------------
        
        # calcul des constantes pour schéma de Newmark
        NK_a = (gamma_NK/beta_NK)*cj + (1./(beta_NK*DT))*mj
        NK_b = DT*(gamma_NK/(2.*beta_NK)-1.)*cj + (1./(2.*beta_NK))*mj
        
        # calcul de la raideur apparente
        Keq = kj + (gamma_NK/(beta_NK*DT))*cj + (1./(beta_NK*DT**2))*mj
        
        for i in range(0,n_step):
            
            # calcul de l'incrément de force
            Dp = pj[i+1]-pj[i]
            Dpeq = Dp + NK_a*Vj[i] + NK_b*Aj[i]
            
            # calcul de l'incrément de déplacement, de vitesse et d'accélération
            DUj = Dpeq/Keq
            DVj = (gamma_NK/(beta_NK*DT))*DUj - (gamma_NK/beta_NK)*Vj[i] + \
                   DT*(1.-(gamma_NK/(2.*beta_NK)))*Aj[i]
            DAj = (1./(beta_NK*DT**2))*DUj - (1./(beta_NK*DT))*Vj[i] - (1./(2.*beta_NK))*Aj[i]
            
            # mise à jour des variables
            Uj[i+1] = Uj[i] + DUj
            Vj[i+1] = Vj[i] + DVj
            Aj[i+1] = Aj[i] + DAj
        
        U += numpy.outer(phi,Uj)
        V += numpy.outer(phi,Vj)
        A += numpy.outer(phi,Aj)
    
    # output des résultats en termes de déplacement, vitesse et accélération
    model.SOLUTION["U"] = assembly.dof2node(model,U,"disp")
    model.SOLUTION["V"] = assembly.dof2node(model,V,"disp")
    model.SOLUTION["A"] = assembly.dof2node(model,A,"disp")
    
    # output du vecteur de chargement utilisé dans le calcul
    model.SOLUTION["F"] = assembly.dof2node(model,F,"force")

