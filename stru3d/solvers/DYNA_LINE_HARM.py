#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# DYNA_LINE_HARM - Algorithme de calcul de la réponse dynamique complexe d'un
# système sousmis à une sollicitation harmonique
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
import scipy
from math import pi
from stru3d.core.assembly import assembly
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS,CATA_ELEM_DAMP

def DYNA_LINE_HARM(model):
    """
    Calcul de la réponse dynamique complexe d'un système soumis à une
    excitation harmonique.
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    NODE_LOADS  = model.NODE_LOADS
    NODE_DISP   = model.NODE_DISP
    LFREQ       = model.LFREQ
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # nombre de fréquences à calculer
    n_freq = len(LFREQ)
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de raideur
    C = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice d'amortissement
    M = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de masse
    F = numpy.zeros((n_dof,n_freq), dtype=numpy.complex64) # vecteur des forces
    U = numpy.zeros((n_dof,n_freq), dtype=numpy.complex64) # vecteur des déplacements
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage de la matrice de masse
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_MASS):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            M_elem = elem.M
            M = assembly.matrix_assembly(M, M_elem, ldof)
    
    # Assemblage de la matrice d'amortissement
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_DAMP):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            C_elem = elem.C
            C = assembly.matrix_assembly(C, C_elem, ldof)
    
    # Calcul de la matrice d'amortissement global de Rayleigh
    if model.DAMPING:
        if model.DAMPING["type"] == "RAYLEIGH":
            w1  = 2.*pi*model.DAMPING["f1"]
            w2  = 2.*pi*model.DAMPING["f2"]
            xi1 = model.DAMPING["xi1"]
            xi2 = model.DAMPING["xi2"]
            a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
            b_ = numpy.array([xi1,xi2])
            a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
            C += a0*M + a1*K
    
    # Assemblage du vecteur de déplacements (déplacements imposés)
    for no in NODE_DISP.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'DX':i0, 'DY':i1, 'DZ':i2, 'RX':i3, 'RY':i4, 'RZ':i5}
        for t in NODE_DISP[no].keys():
            U[dIndex[t]] += NODE_DISP[no][t]
    
    # Assemblage du vecteur de chargement aux noeuds
    for no in NODE_LOADS.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'FX':i0, 'FY':i1, 'FZ':i2, 'MX':i3, 'MY':i4, 'MZ':i5}
        for t in NODE_LOADS[no].keys():
            F[dIndex[t]] += NODE_LOADS[no][t]
    
    for i in range(0,n_freq):
        
        # pulsation de calcul
        omega = 2.*pi*LFREQ[i]
        
        # Matrice d'impédance du système
        A = -omega**2*M + 1j*omega*C + K
        
        # Décomposition de la matrice d'impédance en sous-matrices pour la
        # résolution du système en deux temps (d'abord sur les ddl libres
        # pour calculer leur déplacement et ensuite sur les ddl fixes pour
        # obtenir les réactions aux appuis
        A11 = numpy.delete(numpy.delete(A,DOF['BLOQ'],0),DOF['BLOQ'],1)
        A12 = numpy.delete(numpy.delete(A,DOF['BLOQ'],0),DOF['FREE'],1)
        A21 = numpy.transpose(A12)
        A22 = numpy.delete(numpy.delete(A,DOF['FREE'],0),DOF['FREE'],1)
        
        # Vecteur avec avec le chargement imposé aux ddl libres
        F1 = numpy.delete(F[:,i],DOF['BLOQ'],0)
        
        # Vecteur avec les déplacements imposés aux ddl bloqués
        U2 = numpy.delete(U[:,i],DOF['FREE'],0)
        
        # Calcul des déplacements des ddl libres
        U1 = scipy.linalg.solve(A11,F1-dot(A12,U2))
        
        # Calcul des réactions aux ddl bloqués
        F2 = dot(A21,U1) + dot(A22,U2)
        
        U[DOF['FREE'],i] = U1
        F[DOF['BLOQ'],i] = F2
    
    model.SOLUTION["U"] = assembly.dof2node(model,U,"disp")
    model.SOLUTION["F"] = assembly.dof2node(model,F,"force")

