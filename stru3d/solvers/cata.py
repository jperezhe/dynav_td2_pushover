#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Catalogue des algorithmes de résolution
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

from stru3d.solvers.DYNA_HARM_MODAL import DYNA_HARM_MODAL
from stru3d.solvers.DYNA_LINE import DYNA_LINE
from stru3d.solvers.DYNA_LINE_HARM import DYNA_LINE_HARM
from stru3d.solvers.DYNA_NON_LINE import DYNA_NON_LINE
from stru3d.solvers.DYNA_TRAN_MODAL import DYNA_TRAN_MODAL
from stru3d.solvers.MODES_LINE import MODES_LINE
from stru3d.solvers.STAT_LINE import STAT_LINE
from stru3d.solvers.STAT_NON_LINE import STAT_NON_LINE

CATA_SOLVERS ={
    "DYNA_HARM_MODAL": DYNA_HARM_MODAL,
    "DYNA_LINE": DYNA_LINE,
    "DYNA_LINE_HARM": DYNA_LINE_HARM,
    "DYNA_NON_LINE": DYNA_NON_LINE,
    "DYNA_TRAN_MODAL": DYNA_TRAN_MODAL,
    "MODES_LINE": MODES_LINE,
    "STAT_LINE": STAT_LINE,
    "STAT_NON_LINE": STAT_NON_LINE,
}

# linear resolution schemes
l_elas_solve =[
    "STAT_LINE",
    "MODES_LINE",
    "DYNA_LINE",
    "DYNA_LINE_HARM",
    "DYNA_TRAN_MODAL",
    "DYNA_HARM_MODAL",
]

# non-linear resolution schemes
l_non_elas_solve =[
    "STAT_NON_LINE"
    "DYNA_NON_LINE"
]



# test
if __name__ == '__main__':
    
    pass
    
