#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# STAT_LINE - Algorithme de résolution d'un problème mécanique statique
# linéaire
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from scipy.linalg import inv        # faster than numpy
from stru3d.core.assembly import assembly
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS

def STAT_LINE(model):
    """
    Résolution d'un problème de mécanique statique linéaire.
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    NODE_LOADS  = model.NODE_LOADS
    NODE_DISP   = model.NODE_DISP
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof), dtype=numpy.float64)   # matrice de raideur
    M = numpy.zeros((n_dof,n_dof), dtype=numpy.float64)   # matrice de masse
    F = numpy.zeros(n_dof, dtype=numpy.float64)           # vecteur des forces
    U = numpy.zeros(n_dof, dtype=numpy.float64)           # vecteur des déplacements
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage de la matrice de masse
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_MASS):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            M_elem = elem.M
            M = assembly.matrix_assembly(M, M_elem, ldof)
    
    # Assemblage du vecteur de déplacements (déplacements imposés)
    for no in NODE_DISP.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'DX':i0, 'DY':i1, 'DZ':i2, 'RX':i3, 'RY':i4, 'RZ':i5}
        for t in NODE_DISP[no].keys():
            U[dIndex[t]] += NODE_DISP[no][t]
    
    # Assemblage du vecteur de chargement aux noeuds
    for no in NODE_LOADS.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'FX':i0, 'FY':i1, 'FZ':i2, 'MX':i3, 'MY':i4, 'MZ':i5}
        for t in NODE_LOADS[no].keys():
            F[dIndex[t]] += NODE_LOADS[no][t]
    
    # Force équivalente pour un chargement gravitaire
    if model.GRAVITY:
        dir = model.GRAVITY["direction"]
        idir ={"X":0,"Y":1,"Z":2}
        delta = numpy.zeros(n_dof, dtype=numpy.float)
        for no in DOF["NODE"].keys():
            idof = DOF["NODE"][no][idir[dir]]
            delta[idof] = 1.
        # on reactualise le vecteur de chargement avec la charge équivalente
        # à un chargement de poids propre pour la direction concernée
        F += dot(M,delta)*model.GRAVITY["value"]
    
    
    # Décomposition de la matrice de raideur en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    K11 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['BLOQ'],1)
    K12 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['FREE'],1)
    K21 = numpy.transpose(K12)
    K22 = numpy.delete(numpy.delete(K,DOF['FREE'],0),DOF['FREE'],1)
    
    # Décomposition de la matrice de masse en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    M11 = numpy.delete(numpy.delete(M,DOF['BLOQ'],0),DOF['BLOQ'],1)
    M12 = numpy.delete(numpy.delete(M,DOF['BLOQ'],0),DOF['FREE'],1)
    M21 = numpy.transpose(M12)
    M22 = numpy.delete(numpy.delete(M,DOF['FREE'],0),DOF['FREE'],1)
    
    # Vecteur avec les déplacements imposés aux ddl bloqués
    U2 = numpy.delete(U,DOF['FREE'],0)
    
    # Vecteur avec avec le chargement imposé aux ddl libres
    F1 = numpy.delete(F,DOF['BLOQ'],0)
    
    if len(F1):
        # Calcul des déplacements des ddl libres
        U1 = dot(inv(K11),F1-dot(K12,U2))
        
        # Calcul des réactions aux ddl bloqués
        F2 = dot(K21,U1) + dot(K22,U2)
    
    else:       # nota: particular case where the displacement is imposed at all ddl
        # Déplacements aux ddl libres (none)
        U1 = numpy.zeros(0,dtype=numpy.float64)
        
        # Calcul des réactions aux ddl bloqués (déplacement imposé)
        F2 = dot(K22,U2)
    
    # assemblage des vecteurs de déplacement et des forces de réaction
    U[DOF['FREE']] = U1[:]
    # F[DOF['BLOQ']] += F2[:]   # nota: utiliser "+=" permet de prendre en compte
                              # # dans les forces de réaction les forces appliquées
                              # # directement sur les noeuds fixes
    F[DOF['BLOQ']] = F2[:]
    
    # results
    model.SOLUTION['U'] = assembly.dof2node(model,U,"disp")
    model.SOLUTION['F'] = assembly.dof2node(model,F,"force")

