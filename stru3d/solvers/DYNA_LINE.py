#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# DYNA_LINE - Algorithme de calcul de la réponse dynamique transitoire d'un
# système linéaire soumis à une excitation dynamique
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import array, zeros, dot
from scipy.linalg import inv        # faster than numpy
from math import sin, cos, pi, sqrt
from stru3d.core.assembly import assembly
from stru3d.core.support import interp_data
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS,CATA_ELEM_DAMP

def DYNA_LINE(model):
    """
    Calcul de la réponse dynamique transitoire à une excitation
    temporelle quelconque.
    
    IMPORTANTE: pour utiliser cet algorithme il faut que l'incrément
    de temps soit constant pour tous les pas de temps!
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    NODE_LOADS  = model.NODE_LOADS
    NODE_DISP   = model.NODE_DISP
    TIME        = model.TIME
    DT          = model.DT
    n_step = len(TIME)-1
    
    # paramètres schéma d'intégration de Newmark
    gamma_NK = 1./2.
    beta_NK  = 1./4.
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de raideur
    C = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice d'amortissement
    M = numpy.zeros((n_dof,n_dof),  dtype=numpy.float64)   # matrice de masse
    F = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des forces
    U = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des déplacements
    V = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des vitesses
    A = numpy.zeros((n_dof,n_step+1), dtype=numpy.float64)   # vecteur des accélérations
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage de la matrice de masse
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_MASS):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            M_elem = elem.M
            M = assembly.matrix_assembly(M, M_elem, ldof)
    
    # Assemblage de la matrice d'amortissement
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_DAMP):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            C_elem = elem.C
            C = assembly.matrix_assembly(C, C_elem, ldof)
    
    # Calcul de la matrice d'amortissement global de Rayleigh
    if model.DAMPING:
        if model.DAMPING["type"] == "RAYLEIGH":
            w1  = 2.*pi*model.DAMPING["f1"]
            w2  = 2.*pi*model.DAMPING["f2"]
            xi1 = model.DAMPING["xi1"]
            xi2 = model.DAMPING["xi2"]
            a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
            b_ = numpy.array([xi1,xi2])
            a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
            C += a0*M + a1*K
    
    # Save of system matrices in memory (for post-treatment purpouses)
    model.SOLUTION["C"] = C
    model.SOLUTION["M"] = M
    
    # Assemblage du vecteur de déplacements (déplacements imposés)
    # NOTA: pas utilisé dans cette version de l'algorithme
    
    # Assemblage du vecteur de chargement aux noeuds
    for no in NODE_LOADS.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'FX':i0, 'FY':i1, 'FZ':i2, 'MX':i3, 'MY':i4, 'MZ':i5}
        for t in NODE_LOADS[no].keys():
            F[dIndex[t]] += interp_data(NODE_LOADS[no][t]["time"],NODE_LOADS[no][t]["force"],TIME)
    
    # Force equivalente pour un chargement sismique
    if model.EARTHQUAKE:
        acc = interp_data(model.EARTHQUAKE["time"],model.EARTHQUAKE["acceleration"],TIME)
        model.SOLUTION["ACC_SUPPORT"] = acc          # sauvegarde de l'accélération appliquée au niveau du support
        dir = model.EARTHQUAKE["direction"]
        idir ={"X":0,"Y":1,"Z":2}
        delta = numpy.zeros(n_dof, dtype=numpy.float64)
        for no in DOF["NODE"].keys():
            idof = DOF["NODE"][no][idir[dir]]
            delta[idof] = 1.
        # on reactualise le vecteur de chargement avec la charge équivalente
        # (en realité F est une matrice qui contient les différents vecteurs
        # de chargement pour chaque instant du temps)
        try:
            F += -1.*numpy.outer(dot(M,delta),acc)
        
        except:
            # forces calculation of inertial forces in case of MemoryError
            d_masse = dot(M,delta)
            for i in range(0,len(TIME)):
                F[:,i] += -1.*d_masse*acc[i]
    
    
    # Décomposition de la matrice de raideur en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    K11 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['BLOQ'],1)
    K12 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['FREE'],1)
    K21 = numpy.transpose(K12)
    K22 = numpy.delete(numpy.delete(K,DOF['FREE'],0),DOF['FREE'],1)
    
    # Décomposition de la matrice d'amortissement en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    C11 = numpy.delete(numpy.delete(C,DOF['BLOQ'],0),DOF['BLOQ'],1)
    C12 = numpy.delete(numpy.delete(C,DOF['BLOQ'],0),DOF['FREE'],1)
    C21 = numpy.transpose(C12)
    C22 = numpy.delete(numpy.delete(C,DOF['FREE'],0),DOF['FREE'],1)
    
    # Décomposition de la matrice de masse en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    M11 = numpy.delete(numpy.delete(M,DOF['BLOQ'],0),DOF['BLOQ'],1)
    M12 = numpy.delete(numpy.delete(M,DOF['BLOQ'],0),DOF['FREE'],1)
    M21 = numpy.transpose(M12)
    M22 = numpy.delete(numpy.delete(M,DOF['FREE'],0),DOF['FREE'],1)
    
    # Vecteur avec avec le chargement imposé aux ddl libres
    F1 = numpy.delete(F,DOF['BLOQ'],0)
    
    # Vecteurs de déplacement, vitesse et accélération aux ddl libres
    U1 = numpy.delete(U,DOF['BLOQ'],0)
    V1 = numpy.delete(V,DOF['BLOQ'],0)
    A1 = numpy.delete(A,DOF['BLOQ'],0)
    
    # vecteur des forces internes
    fSj = numpy.zeros(len(DOF['FREE']),dtype=numpy.float64)
    
    # calcul de l'accélération initiale compatible avec les conditions initiales
    # A1[:,0] = dot(inv(M11),(F1[:,0]-dot(C11,V1[:,0])-dot(K11,U1[:,0])))
    # reste à ajouter la possibilité d'insérer A1 dans A
    
    # calcul des constantes pour schéma de Newmark
    NK_a = (gamma_NK/beta_NK)*C11 + (1./(beta_NK*DT))*M11
    NK_b = DT*(gamma_NK/(2.*beta_NK)-1.)*C11 + (1./(2.*beta_NK))*M11
    
    # calcul de la matrice de raideur apparente
    Keq = K11 + (gamma_NK/(beta_NK*DT))*C11 + (1./(beta_NK*DT**2))*M11
    Keq_inv = inv(Keq)
    
    for i in range(0,n_step):
        
        # calcul de l'incrément de force
        Dp = F1[:,i+1]-F1[:,i]
        Dpeq = Dp + dot(NK_a,V1[:,i]) + dot(NK_b,A1[:,i])
        
        # calcul de l'incrément de déplacement, de vitesse et d'accélération
        DU1 = dot(Keq_inv,Dpeq)
        DV1 = (gamma_NK/(beta_NK*DT))*DU1 - (gamma_NK/beta_NK)*V1[:,i] + \
               DT*(1.-(gamma_NK/(2.*beta_NK)))*A1[:,i]
        DA1 = (1./(beta_NK*DT**2))*DU1 - (1./(beta_NK*DT))*V1[:,i] - (1./(2.*beta_NK))*A1[:,i]
        
        # mise à jour des variables
        U1[:,i+1] = U1[:,i] + DU1
        V1[:,i+1] = V1[:,i] + DV1
        A1[:,i+1] = A1[:,i] + DA1
    
    U[DOF['FREE']] = U1
    V[DOF['FREE']] = V1
    A[DOF['FREE']] = A1
    
    # output des résultats en termes de déplacement, vitesse et accélération
    model.SOLUTION["U"] = assembly.dof2node(model,U,"disp")
    model.SOLUTION["V"] = assembly.dof2node(model,V,"disp")
    model.SOLUTION["A"] = assembly.dof2node(model,A,"disp")
    
    # output du vecteur de chargement utilisé dans le calcul
    model.SOLUTION["F"] = assembly.dof2node(model,F,"force")


