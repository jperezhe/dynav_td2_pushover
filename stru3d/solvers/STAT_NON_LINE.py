#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# STAT_NON_LINE - Algorithme de calcul de l'évolution mécanique en
# quasi-statique d'un système non-linéaire
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import array, zeros, dot
from scipy.linalg import inv        # faster than numpy
from math import sqrt
from stru3d.core.assembly import assembly
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS


# on définit une fonction qui vérifie la convergence d'un calcul
# non-linéaire suivant le schéma de NR
def convergence(R0,R,tol):
    """
    Retourne True lorsque le critère de convergence est atteint
    """
    norm_R0 = sqrt(dot(R0.T,R0))
    norm_R  = sqrt(dot(R.T,R))
    if norm_R0 < 1e-30:
        # to avoid division by zero
        # e.g. this would be the case in displacement control analyses
        if norm_R < tol:
            return True
        else:
            return False
    else:
        if (norm_R/norm_R0) < tol:
            return True
        else:
            return False

def STAT_NON_LINE(model):
    """
    Calcul de l'évolution mécanique en quasi-statique d'une structure non-linéaire.
    """
    
    NODE        = model.NODE
    DOF         = model.DOF
    NODE_LOADS  = model.NODE_LOADS
    NODE_DISP   = model.NODE_DISP
    n_step      = model.NSTEP
    TOL         = model.TOL
    
    # nombre de degrés de liberté dans le problème
    n_dof = len(DOF["NODE"].keys())*6
    
    # initialisation des matrices et des vecteurs globaux
    K = numpy.zeros((n_dof,n_dof), dtype=numpy.float)   # matrice de raideur
    F = numpy.zeros((n_dof,n_step+1), dtype=numpy.float)   # vecteur des forces
    U = numpy.zeros((n_dof,n_step+1), dtype=numpy.float)   # vecteur des déplacements
    
    # Assemblage de la matrice de raideur
    for elem in model.ELEM:
        if isinstance(elem,CATA_ELEM_STIFF):
            nodes = elem.nodes
            ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
            K_elem = elem.K
            K = assembly.matrix_assembly(K, K_elem, ldof)
    
    # Assemblage du vecteur de déplacements (déplacements imposés)
    for no in NODE_DISP.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'DX':i0, 'DY':i1, 'DZ':i2, 'RX':i3, 'RY':i4, 'RZ':i5}
        for t in NODE_DISP[no].keys():
            U[dIndex[t]] += NODE_DISP[no][t]
    
    # Assemblage du vecteur de chargement aux noeuds
    for no in NODE_LOADS.keys():
        [i0,i1,i2,i3,i4,i5] = DOF['NODE'][no]
        dIndex = {'FX':i0, 'FY':i1, 'FZ':i2, 'MX':i3, 'MY':i4, 'MZ':i5}
        for t in NODE_LOADS[no].keys():
            # F[dIndex[t]] += NODE_LOADS[no][t]
            # F[dIndex[t]] += numpy.linspace(0.,NODE_LOADS[no][t],n_step+1)
            F[dIndex[t]] += NODE_LOADS[no][t]
    
    # Décomposition de la matrice de raideur en sous-matrices pour la
    # résolution du système en deux temps (d'abord sur les ddl libres
    # pour calculer leur déplacement et ensuite sur les ddl fixes pour
    # obtenir les réactions aux appuis
    K11 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['BLOQ'],1)
    K12 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['FREE'],1)
    K21 = numpy.transpose(K12)
    K22 = numpy.delete(numpy.delete(K,DOF['FREE'],0),DOF['FREE'],1)
    
    # Vecteur avec avec le chargement imposé aux ddl libres
    F1 = numpy.delete(F,DOF['BLOQ'],0)
    
    # Vecteur avec les déplacements imposés aux ddl bloqués
    U2 = numpy.delete(U,DOF['FREE'],0)
    
    # vecteur des forces internes
    fSj = numpy.zeros(len(DOF['FREE']),dtype=numpy.float)
    
    for i in range(0,n_step):
        
        # calcul de l'incrément de chargement pour le pas en cours
        Dp_i = F1[:,i+1] - F1[:,i]
        
        # incrément de déplacement imposé aux noeuds bloqués
        DU_ip = U2[:,i+1] - U2[:,i]
        
        # Newton-Raphson modifié
        j = 0
        R = Dp_i
        Kinv = inv(K11)
        U[:,i+1] = U[:,i]
        U[DOF['BLOQ'],i+1] += DU_ip
        R0 = R
        
        while not convergence(R0,R,TOL) or (j == 0):
            
            # calcul de l'incrément de déplacement
            if j == 0:
                dUj = dot(Kinv,R-dot(K12,DU_ip))
            else:
                dUj = dot(Kinv,R)
            
            # mise à jour du déplacement à la fin du step
            U[DOF['FREE'],i+1] += dUj
            
            # itération sur les éléments pour calcule le vecteur de
            # forces internes et la nouvelle matrice de raideur tangente
            fSj1_all = numpy.zeros(n_dof,dtype=numpy.float)
            K_ = numpy.zeros((n_dof,n_dof), dtype=numpy.float)   # matrice de raideur
            for elem in model.ELEM:
                if isinstance(elem,CATA_ELEM_STIFF):
                    ldof = numpy.concatenate([DOF['NODE'][no] for no in elem.nodes],axis=0)
                    f_int_elem,Ktan_elem = elem.iteration(U=U[ldof,i+1],n_iter=j)
                    fSj1_all[ldof] += f_int_elem
                    K_ = assembly.matrix_assembly(K_, Ktan_elem, ldof)
            fSj1 = fSj1_all[DOF['FREE']]
            
            # calcul de l'incrément de force réel
            Dfj = fSj1 - fSj
            
            # Mise à jour des variables avant vérification de la convergence
            R = R - Dfj
            fSj = fSj1
            
            if model.SCHEME == "NR":
                # on utilise le schéma d'intégration Newton-Raphson classique
                K = K_
                K11 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['BLOQ'],1)
                K12 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['FREE'],1)
                Kinv = inv(K11)
            
            if model.INFO:
                print("pas {} - iteration {} - R = {:.5e}".format(i,j,abs(dot(R.T,R))))
            j += 1
            
            if j > model.NITER_MAX:
                if model.FORCE_RES:
                    # the resolution is forced to continue despite the fact the convergence
                    # has not been reached in the given number of max. iterations
                    break
                else:
                    raise Exception("Error (STAT_NON_LINE): max number of iterations has been reached!")
        
        # mise à jour du vecteur de forces du système
        F[:,i+1] = fSj1_all
        
        # nouvelle matrice de raideur tangente à utiliser dans le prochain step
        K = K_
        K11 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['BLOQ'],1)
        K12 = numpy.delete(numpy.delete(K,DOF['BLOQ'],0),DOF['FREE'],1)
    
    # results
    model.SOLUTION['U'] = assembly.dof2node(model,U,"disp")
    model.SOLUTION['F'] = assembly.dof2node(model,F,"force")
    

