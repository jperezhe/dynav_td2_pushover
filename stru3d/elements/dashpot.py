#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Element discret de type amortisseur visqueux
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   03/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from stru3d.util.geom import geom

class dashpot():
    
    def __init__(self,cx=0.,cy=0.,cz=0.,crx=0.,cry=0.,crz=0.,**kwargs):
        """
        Initialisation d'un élément discret de type amortisseur visqueux
        - in:
            cx,cy,cz        - constants en translation de l'amortisseur visqueux
            crx,cry,crz     - constantes en rotation
            u,v (optional)  - vecteurs directeurs (dans ce cas les constantes
                              sont considérées dans le repère local et la
                              matrice d'amortissement de l'élément doit être
                              transformée au repère global)
        """
        self.cx  = cx
        self.cy  = cy
        self.cz  = cz
        self.crx = crx
        self.cry = cry
        self.crz = crz
        
        if "nodes" in kwargs.keys():
            self.nodes = kwargs["nodes"]
        
        # calcul de la matrice d'amortissement de l'élément discret
        self.C_elem = self.get_C_elem()
        
        # matrice d'amortissement dans le repère global
        if ("u" in kwargs.keys()) and ("v" in kwargs.keys()):
            # transformation de la matrice d'amortissement vers le repère globale
            self.u = kwargs["u"]
            self.v = kwargs["v"]
            self.R = geom.rotation_matrix_axis(self.u,self.v,ndof=12)
            self.C = dot(dot(self.R.T,self.C_elem),self.R)
        else:
            self.C = self.C_elem
    
    def get_C_elem(self):
        """
        Calcul de la matrice d'amortissement
        """
        cx  = self.cx
        cy  = self.cy
        cz  = self.cz
        crx = self.crx
        cry = self.cry
        crz = self.crz
        
        return numpy.array([
                    [ cx, 0., 0.,  0.,  0.,  0.,-cx, 0., 0.,  0.,  0.,  0.],
                    [ 0., cy, 0.,  0.,  0.,  0., 0.,-cy, 0.,  0.,  0.,  0.],
                    [ 0., 0., cz,  0.,  0.,  0., 0., 0.,-cz,  0.,  0.,  0.],
                    [ 0., 0., 0., crx,  0.,  0., 0., 0., 0.,-crx,  0.,  0.],
                    [ 0., 0., 0.,  0., cry,  0., 0., 0., 0.,  0.,-cry,  0.],
                    [ 0., 0., 0.,  0.,  0., crz, 0., 0., 0.,  0.,  0.,-crz],
                    [-cx, 0., 0.,  0.,  0.,  0., cx, 0., 0.,  0.,  0.,  0.],
                    [ 0.,-cy, 0.,  0.,  0.,  0., 0., cy, 0.,  0.,  0.,  0.],
                    [ 0., 0.,-cz,  0.,  0.,  0., 0., 0., cz,  0.,  0.,  0.],
                    [ 0., 0., 0.,-crx,  0.,  0., 0., 0., 0., crx,  0.,  0.],
                    [ 0., 0., 0.,  0.,-cry,  0., 0., 0., 0.,  0., cry,  0.],
                    [ 0., 0., 0.,  0.,  0.,-crz, 0., 0., 0.,  0.,  0., crz]])


# test
if __name__ == '__main__':
    
    print("----c1----")
    c1 = dashpot(cx=1.,cy=2.,cz=3.)
    print(c1.C_elem)
    print(c1.C)
    
    print("----c2----")
    c2 = dashpot(cx=1.,cy=2.,cz=3.,u=(1.,0.,0.),v=(0.,1.,0.))
    print(c2.C_elem)
    print(c2.C)
    
    print("----c3----")
    c3 = dashpot(cx=1.,cy=2.,cz=3.,u=(0.,0.,1.),v=(0.,1.,0.))
    print(c3.C_elem)
    print(c3.C)
    
    print("----c4----")
    c4 = dashpot(cx=1.,cy=2.,cz=3.,u=(0.,1.,0.),v=(0.,0.,-1.))
    print(c4.C_elem)
    print(c4.C)
    

