#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Catalogue d'éléments finis
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

from stru3d.elements.beam import beam
from stru3d.elements.dashpot import dashpot
from stru3d.elements.gyromass import gyromass
from stru3d.elements.mass import mass
from stru3d.elements.pile_head import pile_head
from stru3d.elements.spring import spring
from stru3d.elements.hinged_beam import hinged_beam


CATA_ELEM_STIFF =(
    beam,
    pile_head,
    spring,
    hinged_beam,
)

CATA_ELEM_MASS =(
    beam,
    gyromass,
    mass,
)

CATA_ELEM_DAMP =(
    beam,
    dashpot,
    mass,
    pile_head,
    spring,
)

CATA_ELEM_ALL =(
    beam,
    dashpot,
    gyromass,
    mass,
    pile_head,
    spring,
    hinged_beam,
)

_d_elem ={
    "SPRING": spring,
    "MASS": mass,
    "DASHPOT": dashpot,
    "GYROMASS": gyromass,
    "PILE_HEAD": pile_head,
    "BEAM": beam,
    "HINGED_BEAM": hinged_beam,
}

# linear elements
l_elas_elem =[
    beam,
    spring,
    mass,
    dashpot,
    gyromass,
    pile_head,
]

# non-linear elements
l_non_elas_elem =[
    # springNL,
    hinged_beam,
]

# test
if __name__ == '__main__':
    
    pass
    
