#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Elément discret de type masse ponctuelle
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   03/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi, sqrt
from stru3d.util.geom import geom

class mass():
    
    def __init__(self,mx=0.,my=0.,mz=0.,**kwargs):
        """
        Initialisation d'un élémént discret de masse ponctuelle
        - in:
            mx,my,mz        - masses à translation
            u,v (optional)  - vecteurs directeurs (dans ce cas les masses
                              mx,my,mz sont considérées dans le repère local
                              et la matrice de masse de l'élément doit être
                              transformée au repère global)
        """
        self.mx = mx
        self.my = my
        self.mz = mz
        
        if "nodes" in kwargs.keys():
            self.nodes = kwargs["nodes"]
        
        # données pour calculer la matrice d'amortissement de Rayleigh
        # de l'élément
        if "damping" in kwargs.keys():
            self.damping = kwargs["damping"]
        else:
            self.damping = {}
        
        # calcul de la matrice de masse de l'élément discret
        self.M_elem = self.get_M_elem()
        
        # calcul de la matrice d'amortissement de type Rayleigh
        self.C_elem = self.get_C_elem()
        
        # matrice de masse dans le repère global
        if ("u" in kwargs.keys()) and ("v" in kwargs.keys()):
            # transformation de la matrice de masse vers le repère globale
            self.u = kwargs["u"]
            self.v = kwargs["v"]
            self.R = geom.rotation_matrix_axis(self.u,self.v,ndof=6)
            self.M = dot(dot(self.R.T,self.M_elem),self.R)
            self.C = dot(dot(self.R.T,self.C_elem),self.R)
        else:
            self.M = self.M_elem
            self.C = self.C_elem
    
    def get_M_elem(self):
        """
        Calcul de la matrice de masse
        """
        mx = self.mx
        my = self.my
        mz = self.mz
        return numpy.array([
                    [ mx, 0., 0., 0., 0., 0.],
                    [ 0., my, 0., 0., 0., 0.],
                    [ 0., 0., mz, 0., 0., 0.],
                    [ 0., 0., 0., 0., 0., 0.],
                    [ 0., 0., 0., 0., 0., 0.],
                    [ 0., 0., 0., 0., 0., 0.]])
    
    def get_C_elem(self):
        """
        Calcul de la matrice d'amortissement de Rayleigh de l'élément
        
        Nota: en realité la matrice d'amortissement ne dépend dans ce cas que
        de la matrice de masse parce que l'élément de type masse ponctuelle
        n'a pas de matrice de raideur associée
        """
        if self.damping:
            if self.damping["type"] == "RAYLEIGH":
                w1  = 2.*pi*self.damping["f1"]
                w2  = 2.*pi*self.damping["f2"]
                xi1 = self.damping["xi1"]
                xi2 = self.damping["xi2"]
                a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
                b_ = numpy.array([xi1,xi2])
                a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
        else:
            a0 = 0.
            a1 = 0.
        
        return a0*self.M_elem


# test
if __name__ == '__main__':
    
    print("----m1----")
    m1 = mass(mx=1.,my=2.,mz=3.)
    print(m1.M_elem)
    print(m1.M)
    
    print("----m2----")
    m2 = mass(mx=1.,my=2.,mz=3.,u=(1.,0.,0.),v=(0.,1.,0.))
    print(m2.M_elem)
    print(m2.M)
    
    print("----m3----")
    m3 = mass(mx=1.,my=2.,mz=3.,u=(0.,0.,1.),v=(0.,1.,0.))
    print(m3.M_elem)
    print(m3.M)
    
    print("----m4----")
    m4 = mass(mx=1.,my=2.,mz=3.,u=(0.,1.,0.),v=(0.,0.,-1.))
    print(m4.M_elem)
    print(m4.M)
    

