#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Elément discret de type matrice d'impedance 6x6 à la tête d'un pieu isolé à
# partir de formules analytiques données dans EC8-Partie 5 et Gazetas (1991)
# pour un pieu isolé dans un sol homogène. Se référer à Pérez-Herreros (2020)
# pour plus de détails concernant ces formules.
# 
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   03/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot,exp
from math import pi,sqrt
from stru3d.util.geom import geom

class pile_head():
    
    def __init__(self,**kwargs):
        """
        Initialisation d'un élément discret de type matrice d'impedance en
        tête d'un pieu isolé dans un sol homogène
        
        - in:
            u,v (optional)  - vecteurs directeurs (dans ce cas les rigidités
                              sont considérées dans le repère local et la
                              matrice de raideur de l'élément doit être
                              transformée au repère global)
        
        IMPORTANTE: seul le cas des pieux flottants dans un profil de sol
        constant est traité ici!
        """
        
        if "nodes" in kwargs.keys():
            self.nodes = kwargs["nodes"]
        
        # allows definition of the frequency that is used for the declaration
        # of the element matrix, by default freq=0.
        if "freq" in kwargs.keys():
            self.freq = kwargs["freq"]
        else:
            self.freq = 0.
        
        
        # properties of the piles
        self.d  = kwargs["pile"]["d"]           # pile diameter
        self.L  = kwargs["pile"]["L"]           # pile length
        self.Ep = kwargs["pile"]["Ep"]          # Young modulus
        self.nu = kwargs["pile"]["nu"]          # coefficient de Poisson
        
        # properties of the soil
        self.beta_s = kwargs["soil"]["beta"]    # amortissement hystéretique (ou matériel)
        self.nu_s   = kwargs["soil"]["nu"]      # coefficient de Poisson
        self.rho_s  = kwargs["soil"]["rho"]     # masse volumique
        if "H" in kwargs["soil"].keys():
            self.H = kwargs["soil"]["H"]        # hauteur de la couche de sol
        else:
            self.H = 1000.
        
        if "Es" in kwargs["soil"].keys():
            self.Es = kwargs["soil"]["Es"]
            self.G = self.Es/(2.*(1.+self.nu_s))
            self.Vs = sqrt(self.G/self.rho_s)
        
        elif "Vs" in kwargs["soil"].keys():
            self.Vs = kwargs["soil"]["Vs"]
            self.G = self.rho_s*self.Vs**2
            self.Es = 2.*(1.+self.nu_s)*self.G
        
        elif "G" in kwargs["soil"].keys():
            self.G = kwargs["soil"]["G"]
            self.Vs = sqrt(self.G/self.rho_s)
            self.Es = 2.*(1.+self.nu_s)*self.G
        
        else:
            print("Error(pile_head): no information about soil stiffness is given!")
            raise
        
        # vitesse analogue de Lysmer
        self.VLa = 3.4*self.Vs/(pi*(1-self.nu_s))
        
        # natural frequency of the soil profile under shear deformation
        self.fs = self.Vs/(4.*self.H)
        
        # compression resonant frequency of the soil stratum
        self.fr = self.VLa /(4.*self.H)
        
        
        # calcul de la matrice de raideur de l'élément discret
        self.K_elem = self.get_K_elem(freq=self.freq)
        
        # calcul de la matrice d'amortissement de type Rayleigh
        self.C_elem = self.get_C_elem(freq=self.freq)
        
        # matrice de raideur dans le repère global
        if ("u" in kwargs.keys()) and ("v" in kwargs.keys()):
            # transformation de la matrice de raideur vers le repère globale
            self.u = kwargs["u"]
            self.v = kwargs["v"]
            self.R = geom.rotation_matrix_axis(self.u,self.v,ndof=6)
            self.K = dot(dot(self.R.T,self.K_elem),self.R)
            self.C = dot(dot(self.R.T,self.C_elem),self.R)
        else:
            self.R = geom.rotation_matrix_axis((1.,0.,0.),(0.,1.,0.),ndof=6)
            self.K = self.K_elem
            self.C = self.C_elem
        
        # historique des déplacements et des forces internes à chaque iteration
        # de calcul (ou pas de temps), dans le repère global
        self.history ={
            "U": [],
            "f_int": [],
        }
    
    def get_K_elem(self,freq=0.):
        """
        Calcul de la matrice de raideur
        """
        
        # propriétés du pieu et du sol
        d  = self.d
        L  = self.L
        Ep = self.Ep
        Es = self.Es
        Vs = self.Vs
        
        # head static stiffness terms
        K_h  = 1.08*d*Es*(Ep/Es)**0.21
        K_hm = 0.22*d**2*Es*(Ep/Es)**0.5        # nota: le signe est directement attribué dans la matrice!
        K_m  = 0.16*d**3*Es*(Ep/Es)**0.75
        K_v  = 1.9*d*Es*(L/d)**(2./3.)*(Ep/Es)**(-(L/d)/(Ep/Es))
        
        # dynamic stiffness coefficient for axial response
        if (L/d) < 15.:
            k_v = 1.
        elif (L/d) >= 50.:
            a0 = 2.*pi*freq*d/Vs
            k_v = 1. + sqrt(a0)
        else:
            # linear interpolation between both cases
            a0 = 2.*pi*freq*d/Vs
            k_v = 1. + sqrt(a0)
            k_v = numpy.interp(L/d,[15.,50.],[1.,k_v])
        
        # application of dynamic stiffness coefficients to static stiffness terms
        kh  = 1.*K_h
        khm = 1.*K_hm
        km  = 1.*K_m
        kv  = k_v*K_v
        
        # sauvegarde termes de raideur
        self.K_h  = K_h
        self.K_hm = K_hm
        self.K_m  = K_m
        self.K_v  = K_v
        self.kh  = 1.
        self.khm = 1.
        self.km  = 1.
        self.kv  = k_v
        
        return numpy.array([
                    [  kh,  0., 0.,  0.,-khm, 0.],
                    [  0.,  kh, 0., khm,  0., 0.],
                    [  0.,  0., kv,  0.,  0., 0.],
                    [  0., khm, 0.,  km,  0., 0.],
                    [-khm,  0., 0.,  0.,  km, 0.],
                    [  0.,  0., 0.,  0.,  0., 0.]])
    
    def get_C_elem(self,freq=0.):
        """
        Calcul de la matrice d'amortissement
        """
        
        # properties of pile
        d = self.d
        L  = self.L
        Ep = self.Ep
        
        # properties of soil
        fs = self.fs
        fr = self.fr
        Es = self.Es
        Vs = self.Vs
        rho = self.rho_s
        beta = self.beta_s
        
        # static stiffness terms at pile-head
        K_h  = self.K_h
        K_hm = self.K_hm
        K_m  = self.K_m
        
        # calcul de la pulsation
        if freq < 0.1:
            omega = 2.*pi*0.1       # to avoid division by zero
        else:
            omega = 2.*pi*freq
        
        if freq <= fs:
            xi_h  = 0.8*beta
            xi_hm = 0.5*beta
            xi_m  = 0.25*beta
        else:
            xi_h  = 0.8*beta + 1.1*freq*d*(Ep/Es)**0.17/Vs
            xi_hm = 0.8*beta + 0.85*freq*d*(Ep/Es)**0.18/Vs
            xi_m  = 0.35*beta + 0.35*freq*d*(Ep/Es)**0.2/Vs
        
        # lateral dashpot coefficients
        ch  = 2.*xi_h*K_h/omega
        chm = 2.*xi_hm*K_hm/omega
        cm  = 2.*xi_m*K_m/omega
        
        # axial radiation dashpot coefficient
        if freq <= fr:
            cv = 0.
        elif freq > 1.5*fr:
            rd = 1. - exp(-(Ep/Es)*(L/d)**-2.)
            a0 = omega*d/Vs
            cv = a0**(-1./5.)*rho*Vs*pi*d*L*rd
        else:
            # linear interpolation between both cases
            rd = 1. - exp(-(Ep/Es)*(L/d)**-2.)
            a0 = (2.*pi*1.5)*d/Vs
            cv = a0**(-1./5.)*rho*Vs*pi*d*L*rd
            cv = numpy.interp(freq,[fr,1.5*fr],[0.,cv])
        
        # sauvegarde des variables
        self.xi_h  = xi_h
        self.xi_hm = xi_hm
        self.xi_m  = xi_m
        self.c_h  = ch
        self.c_hm = chm
        self.c_m  = cm
        self.c_v  = cv
        
        return numpy.array([
                    [  ch,  0., 0.,  0.,-chm, 0.],
                    [  0.,  ch, 0., chm,  0., 0.],
                    [  0.,  0., cv,  0.,  0., 0.],
                    [  0., chm, 0.,  cm,  0., 0.],
                    [-chm,  0., 0.,  0.,  cm, 0.],
                    [  0.,  0., 0.,  0.,  0., 0.]])
    
    def get_f_int(self,U_global=None):
        """
        Calcul de la force interne developée dans l'élément par le
        déplacements des noeuds en extremité.
        
        Attention, le vecteur de déplacement est donnée dans le
        repère globale, c'est pour cela que l'on utilise la matrice
        de raideur globale.
        """
        return dot(self.K,U_global)
    
    def update_history(self,n_iter,U,f_int):
        """
        Permet de mettre à jour l'évolution du déplacement et du vecteur
        de forces interne à l'élément
        """
        if n_iter == 0:
            self.history["U"].append([])
            self.history["f_int"].append([])
        self.history["U"][-1] = U
        self.history["f_int"][-1] = f_int
    
    def iteration(self,U=None,**kwargs):
        """
        Résolution pour l'itération en cours
        - in:
            U       - vecteur de déplacements aux noeuds de l'élément
        - out:
            f_int   - vecteur de forces internes à l'élément
            K       - matrice de raideur
        
        IMPORTANTE: cette fonction retourne les éléments dans le repère
        globale
        """
        
        # calcul des forces internes à l'élément
        f_int = self.get_f_int(U_global=U)
        
        # sauvegarde de l'état interne de l'élément
        self.update_history(kwargs["n_iter"],U,f_int)
        
        return f_int, self.K
    
    def update(self,freq=0.,**kwargs):
        """
        Updates the stiffness and the damping matrix of the
        element for the given frequency
        """
        
        # mise à jour de la matrice de raideur
        self.K_elem = self.get_K_elem(freq=freq)
        self.K = dot(dot(self.R.T,self.K_elem),self.R)
        
        # mise à jour de la matrice d'amortissement
        self.C_elem = self.get_C_elem(freq=freq)
        self.C = dot(dot(self.R.T,self.C_elem),self.R)


# test
if __name__ == '__main__':
    
    
    k = pile_head(
                pile={
                    "d" : 1.,
                    "L" : 10.,
                    "Ep": 20000.,
                    "nu": 0.2,
                },
                soil={
                    "Vs": 100.,
                    "nu": 0.3,
                    "rho": 0.0018,          # [kt/m3]
                    "beta": 0.,
                },
                freq = 5.,
    )
    
    print(k.K)
    print(k.C)
    
    pass
    