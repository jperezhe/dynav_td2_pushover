#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Elément discret de type ressort linéaire élastique
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   03/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi
from stru3d.util.geom import geom

class spring():
    
    def __init__(self,kx=0.,ky=0.,kz=0.,krx=0.,kry=0.,krz=0.,**kwargs):
        """
        Initialisation d'un élément discret de type ressort linéaire élastique
        - in:
            kx,ky,kz        - rigidités à translation de l'élément
            krx,kry,krz     - rigidités à rotation
            u,v (optional)  - vecteurs directeurs (dans ce cas les rigidités
                              sont considérées dans le repère local et la
                              matrice de raideur de l'élément doit être
                              transformée au repère global)
        """
        self.kx  = kx
        self.ky  = ky
        self.kz  = kz
        self.krx = krx
        self.kry = kry
        self.krz = krz
        
        # données pour calculer la matrice d'amortissement de Rayleigh
        # de l'élément
        if "damping" in kwargs.keys():
            self.damping = kwargs["damping"]
        else:
            self.damping = {}
        
        if "nodes" in kwargs.keys():
            self.nodes = kwargs["nodes"]
        
        # calcul de la matrice de raideur de l'élément discret
        self.K_elem = self.get_K_elem()
        
        # calcul de la matrice d'amortissement de type Rayleigh
        self.C_elem = self.get_C_elem()
        
        # matrice de raideur dans le repère global
        if ("u" in kwargs.keys()) and ("v" in kwargs.keys()):
            # transformation de la matrice de raideur vers le repère globale
            self.u = kwargs["u"]
            self.v = kwargs["v"]
            self.R = geom.rotation_matrix_axis(self.u,self.v,ndof=12)
            self.K = dot(dot(self.R.T,self.K_elem),self.R)
            self.C = dot(dot(self.R.T,self.C_elem),self.R)
        else:
            self.R = geom.rotation_matrix_axis((1.,0.,0.),(0.,1.,0.),ndof=12)
            self.K = self.K_elem
            self.C = self.C_elem
        
        # historique des déplacements et des forces internes à chaque iteration
        # de calcul (ou pas de temps), dans le repère global
        self.history ={
            "U": [],
            "f_int": [],
        }
    
    def get_K_elem(self):
        """
        Calcul de la matrice de raideur
        """
        kx  = self.kx
        ky  = self.ky
        kz  = self.kz
        krx = self.krx
        kry = self.kry
        krz = self.krz
        
        return numpy.array([
                    [ kx, 0., 0.,  0.,  0.,  0.,-kx, 0., 0.,  0.,  0.,  0.],
                    [ 0., ky, 0.,  0.,  0.,  0., 0.,-ky, 0.,  0.,  0.,  0.],
                    [ 0., 0., kz,  0.,  0.,  0., 0., 0.,-kz,  0.,  0.,  0.],
                    [ 0., 0., 0., krx,  0.,  0., 0., 0., 0.,-krx,  0.,  0.],
                    [ 0., 0., 0.,  0., kry,  0., 0., 0., 0.,  0.,-kry,  0.],
                    [ 0., 0., 0.,  0.,  0., krz, 0., 0., 0.,  0.,  0.,-krz],
                    [-kx, 0., 0.,  0.,  0.,  0., kx, 0., 0.,  0.,  0.,  0.],
                    [ 0.,-ky, 0.,  0.,  0.,  0., 0., ky, 0.,  0.,  0.,  0.],
                    [ 0., 0.,-kz,  0.,  0.,  0., 0., 0., kz,  0.,  0.,  0.],
                    [ 0., 0., 0.,-krx,  0.,  0., 0., 0., 0., krx,  0.,  0.],
                    [ 0., 0., 0.,  0.,-kry,  0., 0., 0., 0.,  0., kry,  0.],
                    [ 0., 0., 0.,  0.,  0.,-krz, 0., 0., 0.,  0.,  0., krz]])
    
    def get_C_elem(self):
        """
        Calcul de la matrice d'amortissement de Rayleigh de l'élément
        
        Nota: en realité la matrice d'amortissement ne dépend dans ce cas que
        de la matrice de raideur parce que l'élément de type ressort n'a pas
        de matrice de masse associée
        """
        if self.damping:
            if self.damping["type"] == "RAYLEIGH":
                w1  = 2.*pi*self.damping["f1"]
                w2  = 2.*pi*self.damping["f2"]
                xi1 = self.damping["xi1"]
                xi2 = self.damping["xi2"]
                a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
                b_ = numpy.array([xi1,xi2])
                a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
        else:
            a0 = 0.
            a1 = 0.
        
        return a1*self.K_elem
    
    def get_f_int(self,U_global=None):
        """
        Calcul de la force interne developée dans l'élément par le
        déplacements des noeuds en extremité.
        
        Attention, le vecteur de déplacement est donnée dans le
        repère globale, c'est pour cela que l'on utilise la matrice
        de raideur globale.
        """
        return dot(self.K,U_global)
    
    def update_history(self,n_iter,U,f_int):
        """
        Permet de mettre à jour l'évolution du déplacement et du vecteur
        de forces interne à l'élément
        """
        if n_iter == 0:
            self.history["U"].append([])
            self.history["f_int"].append([])
        self.history["U"][-1] = U
        self.history["f_int"][-1] = f_int
    
    def iteration(self,U=None,**kwargs):
        """
        Résolution pour l'itération en cours
        - in:
            U       - vecteur de déplacements aux noeuds de l'élément
        - out:
            f_int   - vecteur de forces internes à l'élément
            K       - matrice de raideur
        
        IMPORTANTE: cette fonction retourne les éléments dans le repère
        globale
        """
        
        # calcul des forces internes à l'élément
        f_int = self.get_f_int(U_global=U)
        
        # sauvegarde de l'état interne de l'élément
        self.update_history(kwargs["n_iter"],U,f_int)
        
        return f_int, self.K


# test
if __name__ == '__main__':
    
    print("----k1----")
    k1 = spring(kx=1.,ky=2.,kz=3.)
    print(k1.K_elem)
    print(k1.K)
    
    print("----k2----")
    k2 = spring(kx=1.,ky=2.,kz=3.,u=(1.,0.,0.),v=(0.,1.,0.))
    print(k2.K_elem)
    print(k2.K)
    
    print("----k3----")
    k3 = spring(kx=1.,ky=2.,kz=3.,u=(0.,0.,1.),v=(0.,1.,0.))
    print(k3.K_elem)
    print(k3.K)
    
    print("----k4----")
    k4 = spring(kx=1.,ky=2.,kz=3.,u=(0.,1.,0.),v=(0.,0.,-1.))
    print(k4.K_elem)
    print(k4.K)
    

