#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Elément discret de type gyro-mass. Se référer à "Saitoh, M. Simple model of 
# frequency-dependent impedance functions in soil-structure interaction using 
# frequency-independent elements. Journal of Engineering Mechanics, American 
# Society of Civil Engineers, 2007, 133, 1101-1114" pour plus d'informations
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   03/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import sqrt

# fonction de transformation
def transform_matrix(u,v):
    """
    Assembly transform matrix for global to local coordinate
    transformation
    - in:
        u       - vector of global coords of local x-axis director
        v       - vector of global coords of local y-axis director
    - out:
        T       - transformation matrix
    """
    u = numpy.array(u)
    v = numpy.array(v)
    O = numpy.zeros(3)
    
    ex = u/sqrt(dot(u,u))
    ey = v/sqrt(dot(v,v))
    ez = numpy.cross(ex,ey)
    
    R1 = numpy.hstack((-ex,O,ex,O))
    R2 = numpy.hstack((-ey,O,ey,O))
    R3 = numpy.hstack((-ez,O,ez,O))
    R4 = numpy.hstack((O,-ex,O,ex))
    R5 = numpy.hstack((O,-ey,O,ey))
    R6 = numpy.hstack((O,-ez,O,ez))
    
    return numpy.vstack((R1,R2,R3,R4,R5,R6))


class gyromass():
    
    def __init__(self,eq_m=0.,**kwargs):
        """
        Initialisation d'un discret de type gyro-mass d'après Saitoh (2007)
        - in:
            eq_m    - equivalent mass
            u,v     - vecteurs directeurs
        """
        
        self.eq_m = eq_m
        
        if "nodes" in kwargs.keys():
            self.nodes = kwargs["nodes"]
        
        # calcul de la matrice de masse de l'élément discret
        self.M_elem = self.get_M_elem()
        
        # matrice de raideur dans le repère global
        if ("u" in kwargs.keys()) and ("v" in kwargs.keys()):
            # transformation de la matrice de raideur vers le repère globale
            self.u = kwargs["u"]
            self.v = kwargs["v"]
            self.R = transform_matrix(self.u,self.v)
        else:
            self.R = transform_matrix((1.,0.,0.),(0.,1.,0.))
        self.M = dot(dot(self.R.T,self.M_elem),self.R)
    
    def get_M_elem(self):
        """
        Calcul de la matrice de masse
        """
        eq_m = self.eq_m
        
        return numpy.array([
                    [ eq_m, 0., 0., 0., 0., 0.],
                    [   0., 0., 0., 0., 0., 0.],
                    [   0., 0., 0., 0., 0., 0.],
                    [   0., 0., 0., 0., 0., 0.],
                    [   0., 0., 0., 0., 0., 0.],
                    [   0., 0., 0., 0., 0., 0.]])


# test
if __name__ == '__main__':
    
    pass
    