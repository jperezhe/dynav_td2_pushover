#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Elément de poutre droite de type Euler-Bernouilli. La section est constante
# sur la longueur. Le matériau est homogène, isotrope, élastique linéaire.
#
# Dans cette élément de poutre les hypothèses retenues sont les suivantes:
#   * hypothèse de Navier-Bernouilli: la section droite reste normale à la
#     la déformée de la fibre moyenne.
#   * hypothèse de Saint-Venant: la torsion est libre.
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   03/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi, sqrt
from stru3d.util.geom import geom

class beam():
    
    def __init__(self,geometry=None,material=None,**kwargs):
        """
        Initialisation d'un élément de poutre de type Euler-Bernouilli
        - in:
            nodes       - liste de noeuds définisant la maille support
            dcoords     - dictionnaire qui contient les coordonées des noeuds support
            geometry    - dictionnaire qui contient les propriétés géométriques
                          de la poutre (section, orientation axes local, etc)
                            v       - vecteur qui définit le vecteur y' orthogonal à la
                                      directrice du segment (x'), par proyection dans le
                                      plan orthogonal à la directrice
                            section="general"
                                A       - aire de la section
                                Ix      - moment quadratique de torsion
                                Iy      - moment quadratique de flexion autour axe local y
                                Iz      - moment quadratique de flexion autour axe local z
            
            material    - dictionnaire qui contient les propriétés du matériau
                          élastique utilisé dans la poutre:
                            E   - module d'Young
                            nu  - coefficient de Poisson
                            rho - densité massique
                          Nota: le module de cisaillement G est directement
                          calculé à partir de E et nu, avec: G = E/(2.*(1+nu))
        """
        
        if "nodes" in kwargs.keys():
            self.nodes = kwargs["nodes"]
        if "dcoords" in kwargs.keys():
            self.dcoords = kwargs["dcoords"]
        
        # géométrie de l'élément
        self.p1 = self.dcoords[self.nodes[0]]
        self.p2 = self.dcoords[self.nodes[1]]
        self.v  = geometry['v']
        (x1,y1,z1) = self.p1
        (x2,y2,z2) = self.p2
        self.L = sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2)
        
        if geometry["section"] == "general":
            self.A  = geometry["A"]
            self.Ix = geometry["Ix"]
            self.Iy = geometry["Iy"]
            self.Iz = geometry["Iz"]
        elif geometry["section"] == "circular":
            r = geometry['r']
            self.A  = pi*r**2
            self.Ix = (pi*r**4)/2.
            self.Iy = (pi*r**4)/4.
            self.Iz = (pi*r**4)/4.
        elif geometry["section"] == "rectangle":
            a = geometry['a']
            b = geometry['b']
            self.A  = a*b
            self.Iy = (a**3*b)/12.
            self.Iz = (a*b**3)/12.
            self.Ix = self.Iy + self.Iz
        else:
            raise ValueError("no valid section is defined!")
        
        # propriétés du matériau élastique linéaire
        self.E  = material["E"]
        self.nu = material["nu"]
        if "rho" in material.keys():
            self.rho = material["rho"]
        else:
            self.rho = 0.
        
        # données pour calculer la matrice d'amortissement de Rayleigh
        # de l'élément
        if "damping" in kwargs.keys():
            self.damping = kwargs["damping"]
        else:
            self.damping = {}
        
        # calcul des matrices de raideur, de masse et d'amortissement de l'élément
        self.K_elem = self.get_K_elem()
        self.M_elem = self.get_M_elem()
        self.C_elem = self.get_C_elem()
        
        # calcul de la matrice de rotation (global -> local)
        self.R = geom.rotation_matrix_seg(self.p1,self.p2,self.v)
        
        # matrices de raideur, de masse et d'amortissement dans le repère global
        self.K = dot(dot(self.R.T,self.K_elem),self.R)
        self.M = dot(dot(self.R.T,self.M_elem),self.R)
        self.C = dot(dot(self.R.T,self.C_elem),self.R)
        
        # historique des déplacements et des forces internes à chaque iteration
        # de calcul (ou pas de temps), dans le repère local à l'élément
        self.history ={
            "U": [],
            "f_int": [],
        }
    
    def get_K_elem(self):
        """
        Calcul de la matrice de raideur
        """
        
        # materiau
        E  = self.E
        nu = self.nu
        G  = E/(2.*(1+nu))
        
        # geometrie
        A  = self.A
        L  = self.L
        Ix = self.Ix
        Iy = self.Iy
        Iz = self.Iz
        
        ka = E*A/L
        kb = 12.*E*Iz/L**3
        kc = 6.*E*Iz/L**2
        kd = 12.*E*Iy/L**3
        ke = 6.*E*Iy/L**2
        kf = G*Ix/L
        kg = 4.*E*Iy/L
        kh = 2.*E*Iy/L
        ki = 4.*E*Iz/L
        kj = 2.*E*Iz/L
        
        return numpy.array([
                    [ ka, 0., 0., 0., 0., 0.,-ka, 0., 0., 0., 0., 0.],
                    [ 0., kb, 0., 0., 0., kc, 0.,-kb, 0., 0., 0., kc],
                    [ 0., 0., kd, 0.,-ke, 0., 0., 0.,-kd, 0.,-ke, 0.],
                    [ 0., 0., 0., kf, 0., 0., 0., 0., 0.,-kf, 0., 0.],
                    [ 0., 0.,-ke, 0., kg, 0., 0., 0., ke, 0., kh, 0.],
                    [ 0., kc, 0., 0., 0., ki, 0.,-kc, 0., 0., 0., kj],
                    [-ka, 0., 0., 0., 0., 0., ka, 0., 0., 0., 0., 0.],
                    [ 0.,-kb, 0., 0., 0.,-kc, 0., kb, 0., 0., 0.,-kc],
                    [ 0., 0.,-kd, 0., ke, 0., 0., 0., kd, 0., ke, 0.],
                    [ 0., 0., 0.,-kf, 0., 0., 0., 0., 0., kf, 0., 0.],
                    [ 0., 0.,-ke, 0., kh, 0., 0., 0., ke, 0., kg, 0.],
                    [ 0., kc, 0., 0., 0., kj, 0.,-kc, 0., 0., 0., ki]])
    
    def get_f_int(self,U_local=None):
        """
        Calcul de la force interne
        - in:
            U_local   - vecteur des déplacements aux noeuds de l'élément
        """
        return dot(self.K_elem,U_local)
    
    def get_M_elem(self):
        """
        Calcul de la matrice de masse
        """
        
        # materiau
        rho = self.rho
        
        # geometrie
        A = self.A
        L = self.L
        
        ma = rho*A*L/3.
        mb = rho*A*L/6.
        mc = rho*A*(13.*L)/35.
        md = rho*A*(11.*L**2)/210.
        me = rho*A*(9.*L)/70.
        mf = rho*A*(13.*L**2)/420.
        mg = rho*A*(L**3)/105.
        mh = rho*A*(L**3)/140.
        
        return numpy.array([
                    [ ma, 0., 0., 0., 0., 0., mb, 0., 0., 0., 0., 0.],
                    [ 0., mc, 0., 0., 0., md, 0., me, 0., 0., 0.,-mf],
                    [ 0., 0., mc, 0.,-md, 0., 0., 0., me, 0., mf, 0.],
                    [ 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [ 0., 0.,-md, 0., mg, 0., 0., 0.,-mf, 0.,-mh, 0.],
                    [ 0., md, 0., 0., 0., mg, 0., mf, 0., 0., 0.,-mh],
                    [ mb, 0., 0., 0., 0., 0., ma, 0., 0., 0., 0., 0.],
                    [ 0., me, 0., 0., 0., mf, 0., mc, 0., 0., 0.,-md],
                    [ 0., 0., me, 0.,-mf, 0., 0., 0., mc, 0., md, 0.],
                    [ 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [ 0., 0., mf, 0.,-mh, 0., 0., 0., md, 0., mg, 0.],
                    [ 0.,-mf, 0., 0., 0.,-mh, 0.,-md, 0., 0., 0., mg]])
    
    def get_C_elem(self):
        """
        Calcul de la matrice d'amortissement de l'élément avec
        la formulation de Rayleigh
        """
        if self.damping:
            if self.damping["type"] == "RAYLEIGH":
                w1  = 2.*pi*self.damping["f1"]
                w2  = 2.*pi*self.damping["f2"]
                xi1 = self.damping["xi1"]
                xi2 = self.damping["xi2"]
                a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
                b_ = numpy.array([xi1,xi2])
                a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
        else:
            a0 = 0.
            a1 = 0.
        
        return a0*self.M_elem + a1*self.K_elem
    
    def update_history(self,n_iter,U,f_int):
        """
        Permet de mettre à jour l'évolution du déplacement et du vecteur
        de forces interne à l'élément
        """
        if n_iter == 0:
            self.history["U"].append([])
            self.history["f_int"].append([])
        self.history["U"][-1] = U
        self.history["f_int"][-1] = f_int
    
    def iteration(self,U=None,**kwargs):
        """
        Fait la résolution pour l'itération en cours
        - in:
            U   - vecteur de déplacements aux noeuds de l'élément
        - out:
            f_int   - vecteur de forces internes à l'élément
            K       - matrice de raideur
        
        IMPORTANTE: cette fonction retourne les éléments dans le repère
        globale
        """
        
        # transformation du vecteur de déplacements aux noeuds vers
        # le repère locale de l'élément
        U_local = dot(self.R,U)
        
        # calcul des forces internes à l'élément
        f_int_local = self.get_f_int(U_local=U_local)
        
        # sauvegarde de l'état interne de l'élément
        self.update_history(kwargs["n_iter"],U_local,f_int_local)
        
        # transformation du vecteur de forces internes au repère globale
        f_int = dot(self.R.T,f_int_local)
        
        return f_int, self.K


# test
if __name__ == '__main__':
    
    
    b1 = beam(geometry={
                       "p1":(0.,0.,0.),
                       "p2":(0.,0.,1.),
                       "v":(0.,1.,0.),
                       "section":"general",
                       "A":1.,
                       "Ix":1.,
                       "Iy":1.,
                       "Iz":1.,
             },
             material={"E":1.,"nu":0.})
    
    U = numpy.zeros(12,dtype=numpy.float64)
    U[0] = 1.
    
    f_int,K = b1.iteration(U=U)
    
    U2 = numpy.zeros(12,dtype=numpy.float64)
    U2[2] = 1.
    f_int2,K2 = b1.iteration(U=U2)
    


