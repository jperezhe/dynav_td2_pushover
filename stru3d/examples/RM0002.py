#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de calcul de la réponse d'un modèle rhéologique représentant la
# réponse d'un groupe de pieux 2x2 tel que donné dans Saitoh (2007)
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi,cos
import matplotlib
import matplotlib.pyplot as plt
import copy
import scipy
import scipy.interpolate
from pathlib import Path
from stru3d.core.model import model


def interp1d(x,y,z):
    """
    Interpolation des données 1d
    """
    inter_func = scipy.interpolate.interp1d(x,y)
    return z,inter_func(z)


# run script
if __name__ == "__main__":
    
    
    lfreq = numpy.linspace(0.1,25./pi)
    
    K  = 240000.        # kN/m (static stiffness)
    C  = 0.02*K*1.2
    M  = 0.02**2*K*1.
    k1 = 0.4*K
    c1 = 0.02*k1*0.6
    m1 = 0.02**2*k1*2.3
    k2 = 1.2*K
    c2 = 0.02*k2*1.8
    m2 = 0.02**2*k2*2.5
    
    # modèle rhéologique groupe de pieux 2x2 et s/d=5 (Saitoh2007)
    model_dict_base ={
        "NODE":{
            1:[0.,0.,0.],
            2:[2.,0.,0.],
            3:[1.,0.,0.],
            4:[1.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[1,2],
                3:[1,2],
                
                4:[1,3],
                5:[1,3],
                6:[3,2],
                
                7:[1,4],
                8:[1,4],
                9:[4,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx": K,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[2],
                "DATA":{
                    "cx": C,
                },
            },
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[3],
                "DATA":{
                    "eq_m": M,
                },
            },
            
            #---------
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[4],
                "DATA":{
                    "eq_m": m1,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[5],
                "DATA":{
                    "cx": c1,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[6],
                "DATA":{
                    "kx": k1,
                },
            },
            
            #---------
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[7],
                "DATA":{
                    "eq_m": m2,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[8],
                "DATA":{
                    "cx": c2,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[9],
                "DATA":{
                    "kx": k2,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":4,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        # "NODE_LOADS":[
            # {"NODE":2,"VALUE":{"FX":1.}},
        # ],
        # "TYPE_SOL":"DYNA_LINE_HARM",
        # "LFREQ":lfreq,
        "INFO": False,
    }
    
    
    model_dict = copy.deepcopy(model_dict_base)
    model_dict["NODE_LOADS"] = [{"NODE":2,"VALUE":{"FX":1.}}]
    model_dict["TYPE_SOL"] = "DYNA_LINE_HARM"
    model_dict["LFREQ"] = lfreq
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    U = m.SOLUTION["U"]
    lfreq = m.LFREQ
    limp = 1./U[2]["DX"][:]
    
    fig1,ax1 = plt.subplots(1)
    
    ax1.plot(lfreq,limp.real/1e6,"r-",label="real")
    ax1.plot(lfreq,limp.imag/1e6,"b-",label="imag")

    
    
    
    for freq in [1.,2.,3.,4.,5.,6.,7.,8.]:
    # for freq in [1.,3.,5.,7.]:
        
        print("freq = ",freq)
        
        T = 1/freq
        tfin = 5*T
        dt = 1./(50.*freq)
        
        time = numpy.arange(0.,tfin+dt,dt)
        force = 1.e4*numpy.sin(2.*pi*freq*time)
        
        dyna_load ={
            "time":time,
            "force":force,
        }
        
        model_dict = copy.deepcopy(model_dict_base)
        model_dict["NODE_LOADS"] = [{"NODE":2,"VALUE":{"FX":dyna_load}}]
        model_dict["DT"] = dt                   # pas de temps à utiliser dans la résolution
        model_dict["TFIN"] = 2.*tfin            # temps à la fin de la résolution
        model_dict["TOL"] = 1e-10               # tolerance critère convergence Newton-Raphson modifié
        model_dict["SCHEME"] = "mNR",           # mNR = modified Newton-Raphson
        model_dict["TYPE_SOL"] = "DYNA_LINE"
        
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        U = m.SOLUTION["U"]
        
        max_disp = numpy.max(numpy.abs(U[2]["DX"][int(3.*T/dt):int(5.*T/dt)]))
        
        
        fig2,ax2 = plt.subplots(1)
        ax2.plot(time,force/1.e4*max_disp,"r-")
        ax2.plot(m.TIME,U[2]["DX"],label="{} Hz".format(freq))
        
        # calcul de la partie réelle de l'impedance
        x1,y1 = interp1d(m.TIME,U[2]["DX"],4.25*T)
        K = 1.e4*y1/max_disp**2
        
        # calcul de la partie imaginaire de l'impédance
        x2,y2 = interp1d(m.TIME,U[2]["DX"],4.*T)
        C = -1.e4*y2/max_disp**2
        
        print(K, C)
        
        ax2.plot(x1,y1,"ro")
        ax2.plot(x2,y2,"go")
        
        ax2.set_xlabel("time [s]")
        ax2.set_ylabel("displacement [m]")
        ax2.set_ylim([-0.04,0.04])
        ax2.grid()
        ax2.legend()
        fig2.tight_layout()
        fig2.savefig(u"{}_transient_{}Hz.png".format(Path(__file__).stem,freq))
        
        ax1.plot(freq,K/1e6,"ro")
        ax1.plot(freq,C/1e6,"bo")
    
    
    ax1.set_xlabel("freq [Hz]")
    ax1.set_ylabel("impedance function (kN/m) x1e6")
    ax1.grid()
    ax1.legend(loc=0)
    fig1.tight_layout()
    fig1.savefig(u"{}_impedance.png".format(Path(__file__).stem))
    plt.close()
    
    
    