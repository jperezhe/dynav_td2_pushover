#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution statique d'une poutre soumise à des déplacements
# imposés à ses extrémités
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
import sys
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    # conditions aux limites des cas à tester
    l_cas_disp =[
        {'DX':1, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0},
        {'DX':0, 'DY':1, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0},
        {'DX':0, 'DY':0, 'DZ':1, 'RX':0, 'RY':0, 'RZ':0},
        {'DX':0, 'DY':1, 'DZ':0, 'RX':0, 'RY':0},
        {'DX':0, 'DY':0, 'DZ':1, 'RX':0, 'RZ':0},
    ]
    
    # liste avec les solutions en déplacement cas par cas
    l_disp_test =[
        numpy.array([ 0., 0., 0., 0., 0., 0., 1., 0., 0., 0.,  0. , 0. ]),
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.,  0. , 0. ]),
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 0., 1., 0.,  0. , 0. ]),
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.,  0. , 1.5]),
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., -1.5, 0. ]),
    ]
    
    # idem avec le vecteur de forces
    l_force_test =[
        numpy.array([-200.,  0. ,  0. , 0., 0. ,  0. , 200., 0. , 0. , 0., 0.,  0.]),
        numpy.array([   0., -2. ,  0. , 0., 0. , -1. ,   0., 2. , 0. , 0., 0., -1.]),
        numpy.array([   0.,  0. , -2. , 0., 1. ,  0. ,   0., 0. , 2. , 0., 1.,  0.]),
        numpy.array([   0., -0.5,  0. , 0., 0. , -0.5,   0., 0.5, 0. , 0., 0.,  0.]),
        numpy.array([   0.,  0. , -0.5, 0., 0.5,  0. ,   0., 0. , 0.5, 0., 0.,  0.]),
    ]
    
    
    i = 1
    for cas_disp,disp_test,force_test in zip(l_cas_disp,l_disp_test,l_force_test):
        
        # dictionnaire qui contient le informations du problème à réssoudre
        model_dict ={
            "NODE":{
                1:[0.,0.,0.],
                2:[1.,0.,0.],
            },
            "MESH":{
                "SEG2":{
                    1:[1,2],
                },
            },
            "ELEM":[
                {"ELEM_TYPE":"BEAM",
                    "MESH":[1],
                    "DATA":{
                        "geometry":{
                            "section": "rectangle",
                            "a":0.1,
                            "b":0.1,
                            "v":(0.,1.,0.),
                        },
                        "material":{
                            "E":20000.,
                            "nu":0.2,
                        },
                    },
                },
            ],
            
            # conditions aux limites
            "NODE_DISP":[
                {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":2,"VALUE":cas_disp},
            ],
            "TYPE_SOL":"STAT_LINE",
        }
        
        # résolution du problème
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        U,F = [m.SOLUTION[t] for t in ['U','F']]
        U = assembly.node2dof(m,U,"disp")
        F = assembly.node2dof(m,F,"force")
        
        # print "cas {}".format(i)
        # print "cas_disp node 2\n",cas_disp
        # print "deplacements\n",U
        # print "forces\n",F
        # print "-----------------------"
        
        print("cas {}: ".format(i),((numpy.sum(numpy.abs(U-disp_test )) < 1e-10) and \
                                    (numpy.sum(numpy.abs(F-force_test)) < 1e-10)))
        
        i += 1
    
    
    
    