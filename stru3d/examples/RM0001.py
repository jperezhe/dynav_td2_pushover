#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de calcul de la réponse d'un modèle rhéologique représentant la
# réponse d'un groupe de pieux 2x2 tel que donné dans Saitoh (2007)
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi,cos
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
from stru3d.core.model import model

# run script
if __name__ == "__main__":
    
    
    lfreq = numpy.linspace(0.1,25./pi)
    
    
    # modèle rhéologique groupe de pieux 2x2 et s/d=5 (Saitoh2007)
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[2.,0.,0.],
            3:[1.,0.,0.],
            4:[1.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[1,2],
                3:[1,2],
                
                4:[1,3],
                5:[1,3],
                6:[3,2],
                
                7:[1,4],
                8:[1,4],
                9:[4,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx": 0.6,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[2],
                "DATA":{
                    "cx": 0.0144,
                },
            },
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[3],
                "DATA":{
                    "eq_m": 2.4e-4,
                },
            },
            
            #---------
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[4],
                "DATA":{
                    "eq_m": 2.208e-4,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[5],
                "DATA":{
                    "cx": 2.88e-3,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[6],
                "DATA":{
                    "kx": 0.24,
                },
            },
            
            #---------
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[7],
                "DATA":{
                    "eq_m": 7.2e-4,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[8],
                "DATA":{
                    "cx": 0.02592,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[9],
                "DATA":{
                    "kx": 0.72,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":4,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":2,"VALUE":{"FX":1.}},
        ],
        "TYPE_SOL":"DYNA_LINE_HARM",
        # "LFREQ":[0.5,1.,1.5,2.,2.5,3.,3.5,4.,4.5,5.],
        "LFREQ":lfreq,
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    U = m.SOLUTION["U"]
    # U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
    lfreq = m.LFREQ
    limp = 1./U[2]["DX"][:]
    
    fig, axarr = plt.subplots(2,2)
    
    axarr[0,0].plot(lfreq,limp.real,"ro")
    axarr[0,0].axhline(y=0.,color="k",ls="--")
    axarr[0,0].set_xlabel("freq [Hz]")
    axarr[0,0].set_ylabel(r"$\Re (K)$")
    axarr[0,0].set_ylim([0,2])
    
    axarr[0,1].plot(lfreq,limp.imag/(2.*pi*lfreq*0.02),"ro")
    axarr[0,1].set_xlabel("freq [Hz]")
    axarr[0,1].set_ylabel(r"$\Im (K)/a_0$")
    axarr[0,1].set_ylim([0,4])
    
    axarr[1,0].plot(lfreq,numpy.abs(limp),"ro")
    axarr[1,0].set_xlabel("freq [Hz]")
    axarr[1,0].set_ylabel("complex modulus")
    
    axarr[1,1].plot(lfreq,numpy.abs(numpy.angle(limp)),"ro")
    axarr[1,1].axhline(y=pi/2.,color="k",ls="--")
    axarr[1,1].set_xlabel("freq [Hz]")
    axarr[1,1].set_ylabel("phase angle")
    
    for i in range(0,2):
        for j in range(0,2):
            axarr[i,j].grid()
    
    plt.tight_layout()
    
    fig.savefig(u"{}_imp.png".format(Path(__file__).stem))
    plt.close()
    
    
    
    
