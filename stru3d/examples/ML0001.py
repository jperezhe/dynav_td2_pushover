#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de comparaison de l'algorithme de résolution dynamique linéaire
# (Newmark) par la méthode directe et par superposition modale
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
import matplotlib
import matplotlib.pyplot as plt
import copy
from pathlib import Path
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    
    dyna_load ={
        "time":[1.,1.2,1.4,1.6,1.8,2.],
        "force":[0.,0.6,1.,1.,0.6,0.],
    }
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[1.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
            "POI1":{
                2:[2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx":1.,
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[2],
                "DATA":{
                    "mx":0.159,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":2,"VALUE":{"FX":dyna_load}},
        ],
        "DT":0.05,                      # pas de temps à utiliser dans la résolution
        "TFIN":10.,                     # temps à la fin de la résolution
    }
    
    
    fig_disp, ax_disp = plt.subplots(1)
    fig_vit,  ax_vit  = plt.subplots(1)
    fig_acc,  ax_acc  = plt.subplots(1)
    
    
    # ------ approche direct ----------
    
    model_dict1 = copy.deepcopy(model_dict)
    model_dict1["TYPE_SOL"] = "DYNA_LINE"
    
    # résolution du problème
    m = model(model_dict=model_dict1)
    m.process_DOF()
    m.solve()
    
    TIME = m.TIME
    U = m.SOLUTION["U"]
    V = m.SOLUTION["V"]
    A = m.SOLUTION["A"]
    
    ax_disp.plot(TIME,U[2]["DX"],"-",label="direct")
    ax_vit.plot( TIME,V[2]["DX"],"-",label="direct")
    ax_acc.plot( TIME,A[2]["DX"],"-",label="direct")
    
    
    # ------ résolution par superposition modale --------
    
    model_dict1 = copy.deepcopy(model_dict)
    model_dict1["TYPE_SOL"] = "MODES_LINE"
    
    # résolution du problème
    m = model(model_dict=model_dict1)
    m.process_DOF()
    m.solve()
    
    m.TYPE_SOL = "DYNA_TRAN_MODAL"
    m.DT       = 0.05                       # pas de temps à utiliser dans la résolution
    m.TFIN     = 10.                        # temps à la fin de la résolution
    m.TIME = numpy.arange(0.,m.TFIN+1e-10,m.DT)
    m.solve()
    
    TIME = m.TIME
    U = m.SOLUTION["U"]
    V = m.SOLUTION["V"]
    A = m.SOLUTION["A"]
    
    ax_disp.plot(TIME,U[2]["DX"],"r--",label="super.")
    ax_vit.plot( TIME,V[2]["DX"],"r--",label="super.")
    ax_acc.plot( TIME,A[2]["DX"],"r--",label="super.")
    
    
    
    ax_disp.set_xlabel(u"temps (s)")
    ax_disp.set_ylabel(u"déplacement (m)")
    ax_disp.legend(loc=0)
    ax_disp.grid()
    fig_disp.savefig(u"{}_disp.png".format(Path(__file__).stem))
    
    ax_vit.set_xlabel(u"temps (s)")
    ax_vit.set_ylabel(u"vitesse (m/s)")
    ax_vit.legend(loc=0)
    ax_vit.grid()
    fig_vit.savefig(u"{}_vit.png".format(Path(__file__).stem))
    
    ax_acc.set_xlabel(u"temps (s)")
    ax_acc.set_ylabel(u"accélération (m/s2)")
    ax_acc.legend(loc=0)
    ax_acc.grid()
    fig_acc.savefig(u"{}_acc.png".format(Path(__file__).stem))
    
    plt.close()
    
    