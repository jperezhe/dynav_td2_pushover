#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution avec l'algorithme STAT_NON_LINE d'une poutre de type
# Navier-Bernouilli
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
from stru3d.core.model import model

# test
if __name__ == "__main__":
    
    #----------------------------------
    # modèle A
    #----------------------------------
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,2.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1],
                "DATA":{
                    "geometry":{
                        "section": "circular",
                        "r": 0.5,
                        "v": [1., 0., 0.],
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0., 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{'DX':0.1,'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
        ],
        "NODE_LOADS":[],
        "TYPE_SOL":"STAT_LINE",
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    
    print(u"--------------- model A ---------------")
    print(" * displacements and reaction, node at base")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[1][d_],f_,F[1][f_]))
    print(" * displacements and reaction, node at top")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[2][d_],f_,F[2][f_]))
    
    
    
    
    #----------------------------------
    # modèle B
    #----------------------------------
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,1.],
            3:[0.,0.,2.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[2,3],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1,2],
                "DATA":{
                    "geometry":{
                        "section": "circular",
                        "r": 0.5,
                        "v": [1., 0., 0.],
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0., 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{         'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
            {"NODE":3,"VALUE":{'DX':0.1,'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
        ],
        "NODE_LOADS":[],
        "TYPE_SOL":"STAT_LINE",
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    
    print(u"--------------- model B ---------------")
    print(" * displacements and reaction, node at base")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[1][d_],f_,F[1][f_]))
    print(" * displacements and reaction, node in the middle")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[2][d_],f_,F[2][f_]))
    print(" * displacements and reaction, node at top")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[3][d_],f_,F[3][f_]))
    
    
    
    
    #----------------------------------
    # modèle C
    #----------------------------------
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,2.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1],
                "DATA":{
                    "geometry":{
                        "section": "circular",
                        "r": 0.5,
                        "v": [1., 0., 0.],
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0., 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{'DX':0.1,'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
        ],
        "NODE_LOADS":[],
        "NSTEP":2,              # nombre de pas dans la résolution
        "TOL":1e-10,            # tolerance à utiliser dans le critère de convergence
        "SCHEME":"mNR",         # mNR = modified Newton-Raphson
        "TYPE_SOL":"STAT_NON_LINE",
        "NITER_MAX":10,
        # "INFO":False,
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    
    print(u"--------------- model C ---------------")
    print(" * displacements and reaction, node at base")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[1][d_][-1],f_,F[1][f_][-1]))
    print(" * displacements and reaction, node at top")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[2][d_][-1],f_,F[2][f_][-1]))
    
    
    
    
    
    
    #----------------------------------
    # modèle D
    #----------------------------------
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,2.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1],
                "DATA":{
                    "geometry":{
                        "section": "circular",
                        "r": 0.5,
                        "v": [1., 0., 0.],
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0., 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":2,"VALUE":{'FX':36.815539}},
        ],
        "NSTEP":2,              # nombre de pas dans la résolution
        "TOL":1e-10,            # tolerance à utiliser dans le critère de convergence
        "SCHEME":"mNR",         # mNR = modified Newton-Raphson
        "TYPE_SOL":"STAT_NON_LINE",
        "NITER_MAX":10,
        # "INFO":False,
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    
    print(u"--------------- model D ---------------")
    print(" * displacements and reaction, node at base")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[1][d_][-1],f_,F[1][f_][-1]))
    print(" * displacements and reaction, node at top")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[2][d_][-1],f_,F[2][f_][-1]))
    
    
    
    #----------------------------------
    # modèle E
    #----------------------------------
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,1.],
            3:[0.,0.,2.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[2,3],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1,2],
                "DATA":{
                    "geometry":{
                        "section": "circular",
                        "r": 0.5,
                        "v": [1., 0., 0.],
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0., 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{         'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
            {"NODE":3,"VALUE":{'DX':0.1,'DY':0, 'DZ':0, 'RX':0,         'RZ':0}},
        ],
        "NODE_LOADS":[],
        "NSTEP":2,              # nombre de pas dans la résolution
        "TOL":1e-10,            # tolerance à utiliser dans le critère de convergence
        "SCHEME":"mNR",         # mNR = modified Newton-Raphson
        "TYPE_SOL":"STAT_NON_LINE",
        "NITER_MAX":10,
        # "INFO":False,
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    
    print(u"--------------- model E ---------------")
    print(" * displacements and reaction, node at base")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[1][d_][-1],f_,F[1][f_][-1]))
    print(" * displacements and reaction, node in the middle")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[2][d_][-1],f_,F[2][f_][-1]))
    print(" * displacements and reaction, node at top")
    for d_,f_ in [["DX","FX"],["DY","FY"],["DZ","FZ"],["RX","MX"],["RY","MY"],["RZ","MZ"]]:
        print("    {} = {:11.6f}    {} = {:11.6f}".format(d_,U[3][d_][-1],f_,F[3][f_][-1]))
    
    
    
