#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution statique d'un système avec trois poutres: deux sont
# des poutres très rigides
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
from math import pi
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    U_test = 1.013171       # déplacement en X au noeud 4
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,1.],
            3:[0.,0.,2.],
            4:[0.,0.,3.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[2,3],
                3:[3,4],
            },
        },
        "ELEM":[
            # rigid bars
            {"ELEM_TYPE":"BEAM",
                "MESH":[1,2],
                "DATA":{
                    "geometry":{
                        "section": "general",
                        "A":  1.,        # m^2
                        "Ix": 1.,        # m^4
                        "Iy": 1.,        # m^4
                        "Iz": 1.,        # m^4
                        "v": [0., 1., 0.],
                    },
                    "material":{
                        "E": 2e12,       # kN/m^2
                        "nu": 0.2,
                    },
                },
            },
            # poutre plus souple
            {"ELEM_TYPE":"BEAM",
                "MESH":[3],
                "DATA":{
                    "geometry":{
                        "section": "general",
                        "A":  0.4443,        # m^2
                        "Ix": 2*0.01645,     # m^4
                        "Iy": 0.01645,       # m^4
                        "Iz": 0.01645,       # m^4
                        "v": [0., 1., 0.],
                    },
                    "material":{
                        "E": 2e7,       # kN/m^2
                        "nu": 0.2,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":4,"VALUE":{"FX":1000000.}},
        ],
        "TYPE_SOL":"STAT_LINE",
    }
    
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    U = assembly.node2dof(m,U,"disp")
    F = assembly.node2dof(m,F,"force")
    
    print("cas: {}:".format(1), (abs(U[18] - U_test) < 1e-4))
    
