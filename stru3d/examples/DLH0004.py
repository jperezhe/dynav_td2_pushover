#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de calcul de la réponse harmonique d'un système "nested LPM"
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from math import pi
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    # propriétés du système
    lprop=[
        # ks1 / cs1 / ms1 / alpha
        [20., 0.1732, 0., -1. ],
        [20., 0.1732, 0., -0.5],
        [20., 0.1732, 0.,  0. ],
        [20., 0.1732, 0.,  0.5],
        [20., 0.1732, 0.,  1. ],
    ]
    
    fig, axarr = plt.subplots(2,2)
    
    for ks1,cs1,ms1,alpha_ in lprop:
        
        ks2 = ks1*alpha_
        cs2 = cs1
        ms2 = 0.
        
        # dictionnaire qui contient les informations du problème a réssoudre
        model_dict ={
            "NODE":{
                1:[0.,0.,0.],
                2:[1.,0.,0.],
                3:[2.,0.,0.],
            },
            "MESH":{
                "SEG2":{
                    1:[1,3],
                    2:[1,3],
                    4:[2,3],
                    5:[1,2],
                },
                "POI1":{
                    3:[2],
                    6:[3],
                },
            },
            "ELEM":[
                {"ELEM_TYPE":"SPRING",
                    "MESH":[1],
                    "DATA":{
                        "kx":ks1,
                    },
                },
                {"ELEM_TYPE":"DASHPOT",
                    "MESH":[2],
                    "DATA":{
                        "cx":cs1,
                    },
                },
                {"ELEM_TYPE":"MASS",
                    "MESH":[3],
                    "DATA":{
                        "mx":ms1,
                    },
                },
                {"ELEM_TYPE":"SPRING",
                    "MESH":[4],
                    "DATA":{
                        "kx":ks2,
                    },
                },
                {"ELEM_TYPE":"DASHPOT",
                    "MESH":[5],
                    "DATA":{
                        "cx":cs2,
                    },
                },
                {"ELEM_TYPE":"MASS",
                    "MESH":[6],
                    "DATA":{
                        "mx":ms2,
                    },
                },
            ],
            "NODE_DISP":[
                {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            ],
            "NODE_LOADS":[
                {"NODE":3,"VALUE":{"FX":1.}},
            ],
            "TYPE_SOL":"DYNA_LINE_HARM",
            "LFREQ":[0.5,1.,1.5,2.,2.5,3.,3.5,4.,4.5,5.],
        }
        
        # résolution du problème
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
        lfreq = m.LFREQ
        limp = 1./U[12,:]
        
        axarr[0,0].plot(lfreq,limp.real,"o-",label=r"$\alpha$ = {}".format(alpha_))
        axarr[0,1].plot(lfreq,limp.imag,"o-")
        axarr[1,0].plot(lfreq,numpy.abs(limp),"o-")
        axarr[1,1].plot(lfreq,numpy.abs(numpy.angle(limp)),"o-")
    
    axarr[0,0].axhline(y=0.,color="k",ls="--")
    axarr[0,0].set_xlabel("freq [Hz]")
    axarr[0,0].set_ylabel(r"$\Re (K)$")
    axarr[0,0].legend(loc=0)
    
    axarr[0,1].set_xlabel("freq [Hz]")
    axarr[0,1].set_ylabel(r"$\Im (K)$")
    
    axarr[1,0].set_xlabel("freq [Hz]")
    axarr[1,0].set_ylabel("complex modulus")
    
    axarr[1,1].axhline(y=pi/2.,color="k",ls="--")
    axarr[1,1].set_xlabel("freq [Hz]")
    axarr[1,1].set_ylabel("phase angle")
    
    for i in range(0,2):
        for j in range(0,2):
            axarr[i,j].grid()
    
    plt.tight_layout()
    
    fig.savefig(u"{}_k2.png".format(Path(__file__).stem))
    plt.close()
    
    
    
    # propriétés du système
    lprop=[
        # ks1 / cs1 / ms1 / alpha
        [20., 0.1732, 0., -1. ],
        [20., 0.1732, 0., -0.5],
        [20., 0.1732, 0.,  0. ],
        [20., 0.1732, 0.,  0.5],
        [20., 0.1732, 0.,  1. ],
    ]
    
    fig, axarr = plt.subplots(2,2)
    
    for ks1,cs1,ms1,alpha_ in lprop:
        
        ks2 = ks1
        cs2 = cs1*alpha_
        ms2 = 0.
        
        # dictionnaire qui contient les informations du problème a réssoudre
        model_dict ={
            "NODE":{
                1:[0.,0.,0.],
                2:[1.,0.,0.],
                3:[2.,0.,0.],
            },
            "MESH":{
                "SEG2":{
                    1:[1,3],
                    2:[1,3],
                    4:[2,3],
                    5:[1,2],
                },
                "POI1":{
                    3:[2],
                    6:[3],
                },
            },
            "ELEM":[
                {"ELEM_TYPE":"SPRING",
                    "MESH":[1],
                    "DATA":{
                        "kx":ks1,
                    },
                },
                {"ELEM_TYPE":"DASHPOT",
                    "MESH":[2],
                    "DATA":{
                        "cx":cs1,
                    },
                },
                {"ELEM_TYPE":"MASS",
                    "MESH":[3],
                    "DATA":{
                        "mx":ms1,
                    },
                },
                {"ELEM_TYPE":"SPRING",
                    "MESH":[4],
                    "DATA":{
                        "kx":ks2,
                    },
                },
                {"ELEM_TYPE":"DASHPOT",
                    "MESH":[5],
                    "DATA":{
                        "cx":cs2,
                    },
                },
                {"ELEM_TYPE":"MASS",
                    "MESH":[6],
                    "DATA":{
                        "mx":ms2,
                    },
                },
            ],
            "NODE_DISP":[
                {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            ],
            "NODE_LOADS":[
                {"NODE":3,"VALUE":{"FX":1.}},
            ],
            "TYPE_SOL":"DYNA_LINE_HARM",
            "LFREQ":[0.5,1.,1.5,2.,2.5,3.,3.5,4.,4.5,5.],
        }
        
        # résolution du problème
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
        lfreq = m.LFREQ
        limp = 1./U[12,:]
        
        axarr[0,0].plot(lfreq,limp.real,"o-",label=r"$\alpha$ = {}".format(alpha_))
        axarr[0,1].plot(lfreq,limp.imag,"o-")
        axarr[1,0].plot(lfreq,numpy.abs(limp),"o-")
        axarr[1,1].plot(lfreq,numpy.abs(numpy.angle(limp)),"o-")
    
    axarr[0,0].axhline(y=0.,color="k",ls="--")
    axarr[0,0].set_xlabel("freq [Hz]")
    axarr[0,0].set_ylabel(r"$\Re (K)$")
    axarr[0,0].legend(loc=0)
    
    axarr[0,1].set_xlabel("freq [Hz]")
    axarr[0,1].set_ylabel(r"$\Im (K)$")
    
    axarr[1,0].set_xlabel("freq [Hz]")
    axarr[1,0].set_ylabel("complex modulus")
    
    axarr[1,1].axhline(y=pi/2.,color="k",ls="--")
    axarr[1,1].set_xlabel("freq [Hz]")
    axarr[1,1].set_ylabel("phase angle")
    
    for i in range(0,2):
        for j in range(0,2):
            axarr[i,j].grid()
    
    plt.tight_layout()
    
    fig.savefig(u"{}_c2.png".format(Path(__file__).stem))
    plt.close()
    
