#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution statique linéaire sous chargement gravitaire
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
from stru3d.core.model import model
from stru3d.core.assembly import assembly


# test
if __name__ == "__main__":
    
    # force équivalente et réaction dans à la base du modèle comme résultat
    # du chargement gravitaire
    F_test = numpy.array([300., -300])
    
    
    
    # accélération de la gravité
    # ATTENTION: on la définit avec valeur négative puisque elle
    # est vers les Z négatifs
    gravity = -10.      # m/s2
    
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,1.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
            "POI1":{
                2:[2],
            },
        },
        "ELEM":[
            # poutre structure
            {"ELEM_TYPE":"BEAM",
                "MESH":[1],
                "DATA":{
                    "geometry":{
                        "section": "general",
                        "A":  0.4443,        # m^2
                        "Ix": 2*0.01645,     # m^4
                        "Iy": 0.01645,       # m^4
                        "Iz": 0.01645,       # m^4
                        "v": [0., 1., 0.],
                    },
                    "material":{
                        "E": 2e7,       # kN/m^2
                        "nu": 0.2,
                    },
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[2],
                "DATA":{
                    "mx":30,
                    "my":30,
                    "mz":30,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[],
        "TYPE_SOL":"STAT_LINE",
        "GRAVITY":{
            "value":gravity,
            "direction":"Z",
        }
    }
    
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    U = assembly.node2dof(m,U,"disp")
    F = assembly.node2dof(m,F,"force")
    
    print("U[8]: ",U[8])
    print("F[8]: ",F[8])
    
    print("cas: {}:".format(1), (max(abs(F[[2,8]]-F_test)) < 1e-03))
    

