#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution statique d'une poutre soumise à des déplacements
# imposés à ses extrémités
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[1.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1],
                "DATA":{
                    "geometry":{
                        "v":(0.,1.,0.),
                        "section":"rectangle",
                        "a":0.1,
                        "b":0.1,
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.2,
                    },
                }
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{'DX':0, 'DY':1, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[],
        "TYPE_SOL":"STAT_LINE",
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    U = assembly.node2dof(m,U,"disp")
    F = assembly.node2dof(m,F,"force")
    disp_test = numpy.array([ 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.,  0. , 0. ]),
    force_test = numpy.array([   0., -2. ,  0. , 0., 0. , -1. ,   0., 2. , 0. , 0., 0., -1.]),
    print("cas {}: ".format(1),((numpy.sum(numpy.abs(U-disp_test )) < 1e-10) and \
                                    (numpy.sum(numpy.abs(F-force_test)) < 1e-10)))
    