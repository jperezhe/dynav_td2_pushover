#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution dynamique linéaire (schéma de Newmark)
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    # résultats calcul ABAQUS
    time_ABQ,acc_ABQ,disp_ABQ,vit_ABQ = zip(*[
        #       X                 acc                  disp                vit  
        [      0.        ,         0.          ,       0.           ,      0.         ],
        [     50.E-03    ,         0.          ,       0.           ,      0.         ],
        [    100.E-03    ,         0.          ,       0.           ,      0.         ],
        [    150.E-03    ,         0.          ,       0.           ,      0.         ],
        [    200.E-03    ,         0.          ,       0.           ,      0.         ],
        [    250.E-03    ,         0.          ,       0.           ,      0.         ],
        [    300.E-03    ,         0.          ,       0.           ,      0.         ],
        [    350.E-03    ,         0.          ,       0.           ,      0.         ],
        [    400.E-03    ,         0.          ,       0.           ,      0.         ],
        [    450.E-03    ,         0.          ,       0.           ,      0.         ],
        [    500.E-03    ,         0.          ,       0.           ,      0.         ],
        [    550.E-03    ,         0.          ,       0.           ,      0.         ],
        [    600.E-03    ,         0.          ,       0.           ,      0.         ],
        [    650.E-03    ,         0.          ,       0.           ,      0.         ],
        [    700.E-03    ,         0.          ,       0.           ,      0.         ],
        [    750.E-03    ,         0.          ,       0.           ,      0.         ],
        [    800.E-03    ,         0.          ,       0.           ,      0.         ],
        [    850.E-03    ,         0.          ,       0.           ,      0.         ],
        [    900.E-03    ,         0.          ,       0.           ,      0.         ],
        [    950.E-03    ,         0.          ,       0.           ,      0.         ],
        [      1.        ,         3.96373E-15 ,       2.73125E-18  ,    109.002E-18  ],
        [      1.05      ,       892.552E-03   ,     615.024E-06    ,     24.5452E-03 ],
        [      1.1       ,         1.81795     ,       3.59563E-03  ,     94.6211E-03 ],
        [      1.15      ,         2.71487     ,      11.2171E-03   ,    210.184E-03  ],
        [      1.2       ,         3.56928     ,      25.7087E-03   ,    369.424E-03  ],
        [      1.25      ,         4.0703      ,      48.9867E-03   ,    561.666E-03  ],
        [      1.3       ,         4.49198     ,      82.4484E-03   ,    776.777E-03  ],
        [      1.35      ,         4.84334     ,     127.144E-03    ,      1.01104    ],
        [      1.4       ,         5.11887     ,     183.94E-03     ,      1.26078    ],
        [      1.45      ,         4.71922     ,     253.103E-03    ,      1.50573    ],
        [      1.5       ,         4.21447     ,     333.94E-03     ,      1.72782    ],
        [      1.55      ,         3.64376     ,     425.206E-03    ,      1.92284    ],
        [      1.6       ,         3.01599     ,     525.47E-03     ,      2.08777    ],
        [      1.65      ,         1.74598     ,     632.754E-03    ,      2.20364    ],
        [      1.7       ,       417.419E-03   ,     744.203E-03    ,      2.25441    ],
        [      1.75      ,      -917.666E-03   ,     856.525E-03    ,      2.23856    ],
        [      1.8       ,        -2.23838     ,     966.396E-03    ,      2.15636    ],
        [      1.85      ,        -3.82156     ,       1.07033      ,      2.0009     ],
        [      1.9       ,        -5.36052     ,       1.16453      ,      1.7675     ],
        [      1.95      ,        -6.81554     ,       1.2452       ,      1.45946    ],
        [      2.        ,        -8.16386     ,       1.30873      ,      1.08161    ],
        [      2.05      ,        -8.4918      ,       1.35238      ,    664.397E-03  ],
        [      2.1       ,        -8.63998     ,       1.37488      ,    235.732E-03  ],
        [      2.15      ,        -8.6529      ,       1.37586      ,   -196.622E-03  ],
        [      2.2       ,        -8.53035     ,       1.3553       ,   -625.897E-03  ],
        [      2.25      ,        -8.27424     ,       1.31352      ,     -1.04537    ],
        [      2.3       ,        -7.8886      ,       1.25117      ,     -1.44848    ],
        [      2.35      ,        -7.37945     ,       1.16924      ,     -1.82891    ],
        [      2.4       ,        -6.75477     ,       1.069        ,     -2.1807     ],
        [      2.45      ,        -6.02434     ,     952.022E-03    ,     -2.49835    ],
        [      2.5       ,        -5.1996      ,     820.142E-03    ,     -2.77689    ],
        [      2.55      ,        -4.29345     ,     675.423E-03    ,     -3.01195    ],
        [      2.6       ,        -3.32009     ,     520.129E-03    ,     -3.19986    ],
        [      2.65      ,        -2.29474     ,     356.693E-03    ,     -3.33766    ],
        [      2.7       ,        -1.23348     ,     187.672E-03    ,     -3.42321    ],
        [      2.75      ,      -152.907E-03   ,      15.7143E-03   ,     -3.45517    ],
        [      2.8       ,       930.057E-03   ,    -156.489E-03    ,     -3.43304    ],
        [      2.85      ,         1.99846     ,    -326.242E-03    ,     -3.35715    ],
        [      2.9       ,         3.03557     ,    -490.887E-03    ,     -3.22871    ],
        [      2.95      ,         4.02515     ,    -647.846E-03    ,     -3.04972    ],
        [      3.        ,         4.95171     ,    -794.662E-03    ,     -2.82298    ],
        [      3.05      ,         5.80074     ,    -929.037E-03    ,     -2.55205    ],
        [      3.1       ,         6.55896     ,      -1.04887      ,     -2.24116    ],
        [      3.15      ,         7.21448     ,      -1.15227      ,     -1.89518    ],
        [      3.2       ,         7.75706     ,      -1.23764      ,     -1.51954    ],
        [      3.25      ,         8.17818     ,      -1.30363      ,     -1.1201     ],
        [      3.3       ,         8.47127     ,      -1.34921      ,   -703.135E-03  ],
        [      3.35      ,         8.63173     ,      -1.37367      ,   -275.159E-03  ],
        [      3.4       ,         8.65705     ,      -1.37662      ,    157.124E-03  ],
        [      3.45      ,         8.54684     ,      -1.35802      ,    586.946E-03  ],
        [      3.5       ,         8.30282     ,      -1.31816      ,      1.00758    ],
        [      3.55      ,         7.92881     ,      -1.25766      ,      1.41243    ],
        [      3.6       ,         7.43066     ,      -1.17747      ,      1.79517    ],
        [      3.65      ,         6.81619     ,      -1.07884      ,      2.14981    ],
        [      3.7       ,         6.095       ,    -963.329E-03    ,      2.47079    ],
        [      3.75      ,         5.27838     ,    -832.733E-03    ,      2.75308    ],
        [      3.8       ,         4.37914     ,    -689.101E-03    ,      2.99227    ],
        [      3.85      ,         3.41133     ,    -534.681E-03    ,      3.18461    ],
        [      3.9       ,         2.39012     ,    -371.889E-03    ,      3.32709    ],
        [      3.95      ,         1.33149     ,    -203.277E-03    ,      3.41749    ],
        [      4.        ,       252.022E-03   ,     -31.4817E-03   ,      3.45438    ],
        [      4.05      ,      -831.392E-03   ,     140.806E-03    ,      3.43718    ],
        [      4.1       ,        -1.90179     ,     310.888E-03    ,      3.36618    ],
        [      4.15      ,        -2.9424      ,     476.103E-03    ,      3.24247    ],
        [      4.2       ,        -3.93695     ,     633.863E-03    ,      3.068      ],
        [      4.25      ,        -4.86986     ,     781.699E-03    ,      2.8455     ],
        [      4.3       ,        -5.72652     ,     917.297E-03    ,      2.57845    ],
        [      4.35      ,        -6.49353     ,       1.03853      ,      2.27103    ],
        [      4.4       ,        -7.15887     ,       1.14351      ,      1.92806    ],
        [      4.45      ,        -7.71212     ,       1.23058      ,      1.5549     ],
        [      4.5       ,        -8.14464     ,       1.29839      ,      1.1574     ],
        [      4.55      ,        -8.44964     ,       1.34587      ,    741.779E-03  ],
        [      4.6       ,        -8.62236     ,       1.37228      ,    314.547E-03  ],
        [      4.65      ,        -8.66008     ,       1.3772       ,   -117.608E-03  ],
        [      4.7       ,        -8.56222     ,       1.36056      ,   -547.921E-03  ],
        [      4.75      ,        -8.33031     ,       1.32262      ,   -969.654E-03  ],
        [      4.8       ,        -7.96797     ,       1.26398      ,     -1.3762     ],
        [      4.85      ,        -7.4809      ,       1.18554      ,     -1.76121    ],
        [      4.9       ,        -6.8767      ,       1.08855      ,     -2.11864    ],
        [      4.95      ,        -6.16484     ,     974.508E-03    ,     -2.4429     ],
        [      5.        ,        -5.35647     ,     845.215E-03    ,     -2.72891    ],
        [      5.05      ,        -4.46424     ,     702.688E-03    ,     -2.9722     ],
        [      5.1       ,        -3.50212     ,     549.161E-03    ,     -3.16895    ],
        [      5.15      ,        -2.48518     ,     387.037E-03    ,     -3.31609    ],
        [      5.2       ,        -1.42932     ,     218.853E-03    ,     -3.41131    ],
        [      5.25      ,      -351.096E-03   ,      47.2438E-03   ,     -3.45313    ],
        [      5.3       ,       732.624E-03   ,    -125.105E-03    ,     -3.44088    ],
        [      5.35      ,         1.80487     ,    -295.494E-03    ,     -3.37476    ],
        [      5.4       ,         2.84886     ,    -461.257E-03    ,     -3.25581    ],
        [      5.45      ,         3.84824     ,    -619.798E-03    ,     -3.08588    ],
        [      5.5       ,         4.78737     ,    -768.634E-03    ,     -2.86765    ],
        [      5.55      ,         5.65155     ,    -905.437E-03    ,     -2.60451    ],
        [      5.6       ,         6.42725     ,      -1.02806      ,     -2.3006     ],
        [      5.65      ,         7.10231     ,      -1.13459      ,     -1.96068    ],
        [      5.7       ,         7.66619     ,      -1.22336      ,     -1.59005    ],
        [      5.75      ,         8.11003     ,      -1.29298      ,     -1.19454    ],
        [      5.8       ,         8.42691     ,      -1.34235      ,   -780.323E-03  ],
        [      5.85      ,         8.61185     ,      -1.3707       ,   -353.892E-03  ],
        [      5.9       ,         8.66197     ,      -1.3776       ,     78.0791E-03 ],
        [      5.95      ,         8.57647     ,      -1.36292      ,    508.826E-03  ],
        [      6.        ,         8.3567      ,      -1.32691      ,    931.606E-03  ],
        [      6.05      ,         8.00609     ,      -1.27013      ,      1.3398     ],
        [      6.1       ,         7.53015     ,      -1.19346      ,      1.72702    ],
        [      6.15      ,         6.93631     ,      -1.09811      ,      2.08719    ],
        [      6.2       ,         6.23388     ,    -985.56E-03     ,      2.41469    ],
        [      6.25      ,         5.43386     ,    -857.584E-03    ,      2.70438    ],
        [      6.3       ,         4.54876     ,    -716.183E-03    ,      2.95174    ],
        [      6.35      ,         3.59245     ,    -563.569E-03    ,      3.15288    ],
        [      6.4       ,         2.5799      ,    -402.132E-03    ,      3.30465    ],
        [      6.45      ,         1.52696     ,    -234.4E-03      ,      3.40469    ],
        [      6.5       ,       450.118E-03   ,     -62.9987E-03   ,      3.45143    ],
        [      6.55      ,      -633.768E-03   ,     109.388E-03    ,      3.44413    ],
        [      6.6       ,        -1.70773     ,     280.063E-03    ,      3.3829     ],
        [      6.65      ,        -2.75495     ,     446.352E-03    ,      3.26872    ],
        [      6.7       ,        -3.75904     ,     605.652E-03    ,      3.10336    ],
        [      6.75      ,        -4.70427     ,     755.47E-03     ,      2.88941    ],
        [      6.8       ,        -5.57585     ,     893.46E-03     ,      2.63023    ],
        [      6.85      ,        -6.36013     ,       1.01746      ,      2.32987    ],
        [      6.9       ,        -7.04484     ,       1.12553      ,      1.99304    ],
        [      6.95      ,        -7.61925     ,       1.21598      ,      1.625      ],
        [      7.        ,        -8.07437     ,       1.28739      ,      1.23152    ],
        [      7.05      ,        -8.40307     ,       1.33865      ,    818.762E-03  ],
        [      7.1       ,        -8.60022     ,       1.36895      ,    393.187E-03  ],
        [      7.15      ,        -8.66272     ,       1.37782      ,    -38.5429E-03 ],
        [      7.2       ,        -8.5896      ,       1.36511      ,   -469.668E-03  ],
        [      7.25      ,        -8.38199     ,       1.33103      ,   -893.439E-03  ],
        [      7.3       ,        -8.04316     ,       1.27612      ,     -1.30322    ],
        [      7.35      ,        -7.57841     ,       1.20122      ,     -1.6926     ],
        [      7.4       ,        -6.99501     ,       1.10752      ,     -2.05547    ],
        [      7.45      ,        -6.3021      ,     996.482E-03    ,     -2.38617    ],
        [      7.5       ,        -5.51052     ,     869.841E-03    ,     -2.67951    ],
        [      7.55      ,        -4.63267     ,     729.582E-03    ,     -2.93089    ],
        [      7.6       ,        -3.6823      ,     577.902E-03    ,     -3.13639    ],
        [      7.65      ,        -2.67428     ,     417.174E-03    ,     -3.29278    ],
        [      7.7       ,        -1.62439     ,     249.915E-03    ,     -3.39763    ],
        [      7.75      ,      -549.074E-03   ,      78.7442E-03   ,     -3.44928    ],
        [      7.8       ,       534.835E-03   ,     -93.659E-03    ,     -3.44692    ],
        [      7.85      ,         1.61037     ,    -264.595E-03    ,     -3.3906     ],
        [      7.9       ,         2.66069     ,    -431.389E-03    ,     -3.2812     ],
        [      7.95      ,         3.66935     ,    -591.428E-03    ,     -3.12043    ],
        [      8.        ,         4.62056     ,    -742.207E-03    ,     -2.9108     ],
        [      8.05      ,         5.49942     ,    -881.366E-03    ,     -2.65561    ],
        [      8.1       ,         6.29219     ,      -1.00673      ,     -2.35883    ],
        [      8.15      ,         6.98644     ,      -1.11632      ,     -2.02513    ],
        [      8.2       ,         7.57131     ,      -1.20844      ,     -1.65973    ],
        [      8.25      ,         8.03764     ,      -1.28165      ,     -1.26834    ],
        [      8.3       ,         8.37814     ,      -1.33478      ,   -857.091E-03  ],
        [      8.35      ,         8.58746     ,      -1.36702      ,   -432.428E-03  ],
        [      8.4       ,         8.66234     ,      -1.37785      ,   -995.578E-06  ],
        [      8.45      ,         8.6016      ,      -1.36712      ,    430.451E-03  ],
        [      8.5       ,         8.40619     ,      -1.33498      ,    855.157E-03  ],
        [      8.55      ,         8.07918     ,      -1.28194      ,      1.26647    ],
        [      8.6       ,         7.62568     ,      -1.20883      ,      1.65796    ],
        [      8.65      ,         7.05279     ,      -1.11679      ,      2.02349    ],
        [      8.7       ,         6.36948     ,      -1.00727      ,      2.35734    ],
        [      8.75      ,         5.58646     ,    -881.983E-03    ,      2.65428    ],
        [      8.8       ,         4.71598     ,    -742.885E-03    ,      2.90966    ],
        [      8.85      ,         3.77166     ,    -592.158E-03    ,      3.1195     ],
        [      8.9       ,         2.7683      ,    -432.16E-03     ,      3.28049    ],
        [      8.95      ,         1.7216      ,    -265.396E-03    ,      3.39012    ],
        [      9.        ,       647.951E-03   ,     -94.4784E-03   ,      3.44667    ],
        [      9.05      ,      -435.84E-03    ,      77.9183E-03   ,      3.44926    ],
        [      9.1       ,        -1.5128      ,     249.095E-03    ,      3.39786    ],
        [      9.15      ,        -2.56608     ,     416.371E-03    ,      3.29325    ],
        [      9.2       ,        -3.57918     ,     577.128E-03    ,      3.13709    ],
        [      9.25      ,        -4.53624     ,     728.848E-03    ,      2.93181    ],
        [      9.3       ,        -5.42228     ,     869.158E-03    ,      2.68063    ],
        [      9.35      ,        -6.22342     ,     995.86E-03     ,      2.38748    ],
        [      9.4       ,        -6.92713     ,       1.10697      ,      2.05696    ],
        [      9.45      ,        -7.52239     ,       1.20075      ,      1.69424    ],
        [      9.5       ,        -7.99987     ,       1.27573      ,      1.30499    ],
        [      9.55      ,        -8.35211     ,       1.33074      ,    895.305E-03  ],
        [      9.6       ,        -8.57358     ,       1.36491      ,    471.609E-03  ],
        [      9.65      ,        -8.66082     ,       1.37771      ,     40.5312E-03 ],
        [      9.7       ,        -8.61247     ,       1.36894      ,   -391.18E-03   ],
        [      9.75      ,        -8.42929     ,       1.33875      ,   -816.766E-03  ],
        [      9.8       ,        -8.11413     ,       1.28759      ,     -1.22956    ],
        [      9.85      ,        -7.67194     ,       1.21627      ,     -1.62311    ],
        [      9.9       ,        -7.10964     ,       1.12591      ,     -1.99124    ],
        [      9.95      ,        -6.43603     ,       1.01793      ,     -2.3282     ],
        [     10.        ,        -5.66166     ,     894.008E-03    ,     -2.62871    ]
    ])
    
    
    
    
    
    dyna_load ={
        "time":[1.,1.2,1.4,1.6,1.8,2.],
        "force":[0.,0.6,1.,1.,0.6,0.],
    }
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[1.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
            },
            "POI1":{
                2:[2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx":1.,
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[2],
                "DATA":{
                    "mx":0.159,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":2,"VALUE":{"FX":dyna_load}},
        ],
        "DT":0.05,                      # pas de temps à utiliser dans la résolution
        "TFIN":10.,                     # temps à la fin de la résolution
        "TYPE_SOL":"DYNA_LINE",
    }
    
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
    V = assembly.node2dof(m,m.SOLUTION["V"],"disp")
    A = assembly.node2dof(m,m.SOLUTION["A"],"disp")
    TIME = m.TIME
    
    # on compare l'erreur commis entre les deux résultats (STRU3D vs ABAQUS)
    print("cas: {}:".format(1), (max(numpy.abs(U[6,:]-disp_ABQ))/max(U[6,:]) < 1e-02) and \
                                (max(numpy.abs(V[6,:]-vit_ABQ))/max(V[6,:]) < 1e-02) and \
                                (max(numpy.abs(A[6,:]-acc_ABQ))/max(A[6,:]) < 1e-02))
    
    fig_disp, ax_disp = plt.subplots(1)
    ax_disp.plot(TIME,U[6,:],"-",label="STRU3D")
    ax_disp.plot(time_ABQ,disp_ABQ,"r--",label="ABAQUS")
    ax_disp.set_xlabel("Temps (s)")
    ax_disp.set_ylabel("Déplacement (m)")
    ax_disp.legend(loc=0)
    ax_disp.grid()
    fig_disp.savefig(u"{}_disp.png".format(Path(__file__).stem))
    
    fig_vit, ax_vit = plt.subplots(1)
    ax_vit.plot(TIME,V[6,:],"-",label="STRU3D")
    ax_vit.plot(time_ABQ,vit_ABQ,"r--",label="ABAQUS")
    ax_vit.set_xlabel("Temps (s)")
    ax_vit.set_ylabel("Vitesse (m/s)")
    ax_vit.legend(loc=0)
    ax_vit.grid()
    fig_vit.savefig(u"{}_vit.png".format(Path(__file__).stem))
    
    fig_acc, ax_acc = plt.subplots(1)
    ax_acc.plot(TIME,A[6,:],"-",label="STRU3D")
    ax_acc.plot(time_ABQ,acc_ABQ,"r--",label="ABAQUS")
    ax_acc.set_xlabel("Temps (s)")
    ax_acc.set_ylabel("Accélération (m/s^2)")
    ax_acc.legend(loc=0)
    ax_acc.grid()
    fig_acc.savefig(u"{}_acc.png".format(Path(__file__).stem))
    
    plt.close()
    
    