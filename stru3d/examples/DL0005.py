#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple d'utilisation de la fonctionnalité de sauvegarde d'un modèle STRU3D
# sous forme d'un fichier binaire
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
import matplotlib
import matplotlib.pyplot as plt
import pickle
from pathlib import Path
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    dyna_load ={
        "time":[0.4,0.6,0.8,1.,1.2,1.4],
        "force":[0.,0.6,1.,1.,0.6,0.],
    }
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "MAIL":"DL0004.mail",
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "GROUP_MA":"RESSORT",
                "DATA":{
                    "kx":1.,
                },
            },
            {"ELEM_TYPE":"MASS",
                "GROUP_MA":"MASSE",
                "DATA":{
                    "mx":0.159,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":2,"VALUE":{"FX":dyna_load}},
        ],
        "DT":0.05,                      # pas de temps à utiliser dans la résolution
        "TFIN":20.,                     # temps à la fin de la résolution
        "DAMPING":{
            "type":"RAYLEIGH",
            "f1":0.3,
            "f2":1.,
            "xi1":0.05,
            "xi2":0.05
        },
        "TYPE_SOL":"DYNA_LINE",
    }
    
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    m.save_data("DL0005.pickle")
    
    U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
    V = assembly.node2dof(m,m.SOLUTION["V"],"disp")
    A = assembly.node2dof(m,m.SOLUTION["A"],"disp")
    TIME = m.TIME
    
    fig_disp, ax_disp = plt.subplots(1)
    fig_vit, ax_vit = plt.subplots(1)
    fig_acc, ax_acc = plt.subplots(1)
    
    ax_disp.plot(TIME,U[6,:],"-")
    ax_vit.plot(TIME,V[6,:],"-")
    ax_acc.plot(TIME,A[6,:],"-")
    
    
    with open("DL0005.pickle","rb") as input_file:
        m2 = pickle.load(input_file)
    
    U = assembly.node2dof(m2,m2.SOLUTION["U"],"disp")
    V = assembly.node2dof(m2,m2.SOLUTION["V"],"disp")
    A = assembly.node2dof(m2,m2.SOLUTION["A"],"disp")
    TIME = m2.TIME
    
    ax_disp.plot(TIME,U[6,:],"--")
    ax_vit.plot(TIME,V[6,:],"--")
    ax_acc.plot(TIME,A[6,:],"--")
    
    ax_disp.set_xlabel(u"Temps (s)")
    ax_disp.set_ylabel(u"Déplacement (m)")
    ax_disp.grid()
    fig_disp.savefig(u"{}_disp.png".format(Path(__file__).stem))
    
    ax_vit.set_xlabel(u"Temps (s)")
    ax_vit.set_ylabel(u"Vitesse (m/s)")
    ax_vit.grid()
    fig_vit.savefig(u"{}_vit.png".format(Path(__file__).stem))
    
    ax_acc.set_xlabel(u"Temps (s)")
    ax_acc.set_ylabel(u"Accélération (m/s2)")
    ax_acc.grid()
    fig_acc.savefig(u"{}_acc.png".format(Path(__file__).stem))
    
    plt.close()
    
    