#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de calcul de la réponse d'un modèle rhéologique représentant la
# réponse d'un groupe de pieux 2x2 tel que donné dans Saitoh (2007). Dans ce
# cas on applique un chargement cyclique avec changement de la fréquence de
# sollicitation en cours du temps pour étudier la réponse du système
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi,cos
import matplotlib
import matplotlib.pyplot as plt
import copy
import scipy
import scipy.interpolate
from pathlib import Path
from stru3d.core.model import model

def interp1d(x,y,z):
    """
    Interpolation des données 1d
    """
    inter_func = scipy.interpolate.interp1d(x,y)
    return z,inter_func(z)


# run script
if __name__ == "__main__":
    
    
    lfreq = numpy.linspace(0.1,25./pi)
    
    K  = 240000.        # kN/m (static stiffness)
    C  = 0.02*K*1.2
    M  = 0.02**2*K*1.
    k1 = 0.4*K
    c1 = 0.02*k1*0.6
    m1 = 0.02**2*k1*2.3
    k2 = 1.2*K
    c2 = 0.02*k2*1.8
    m2 = 0.02**2*k2*2.5
    
    # modèle rhéologique groupe de pieux 2x2 et s/d=5 (Saitoh2007)
    model_dict_base ={
        "NODE":{
            1:[0.,0.,0.],
            2:[2.,0.,0.],
            3:[1.,0.,0.],
            4:[1.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[1,2],
                3:[1,2],
                
                4:[1,3],
                5:[1,3],
                6:[3,2],
                
                7:[1,4],
                8:[1,4],
                9:[4,2],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx": K,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[2],
                "DATA":{
                    "cx": C,
                },
            },
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[3],
                "DATA":{
                    "eq_m": M,
                },
            },
            
            #---------
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[4],
                "DATA":{
                    "eq_m": m1,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[5],
                "DATA":{
                    "cx": c1,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[6],
                "DATA":{
                    "kx": k1,
                },
            },
            
            #---------
            {"ELEM_TYPE":"GYROMASS",
                "MESH":[7],
                "DATA":{
                    "eq_m": m2,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[8],
                "DATA":{
                    "cx": c2,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[9],
                "DATA":{
                    "kx": k2,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":4,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "INFO": False,
    }
    
    
    dt = 1./(50.*8)
    
    time = numpy.arange(0.,50.+dt,dt)
    force = 1.e4*numpy.sin(2.*pi*(1.+0.14*time)*time)
    
    
    print(1.+0.14*time)
    
    
    dyna_load ={
        "time":time,
        "force":force,
    }
    
    model_dict = copy.deepcopy(model_dict_base)
    model_dict["NODE_LOADS"] = [{"NODE":2,"VALUE":{"FX":dyna_load}}]
    model_dict["DT"] = dt                   # pas de temps à utiliser dans la résolution
    model_dict["TFIN"] = 55.                # temps à la fin de la résolution
    model_dict["TOL"] = 1e-10               # tolerance critère convergence Newton-Raphson modifié
    model_dict["SCHEME"] = "mNR",           # mNR = modified Newton-Raphson
    model_dict["TYPE_SOL"] = "DYNA_LINE"
    
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    U = m.SOLUTION["U"]
    
    
    max_disp = numpy.max(numpy.abs(U[2]["DX"]))
    
    fig1,ax1 = plt.subplots(1)
    ax1.plot(time,force/1.e4*max_disp,"r-")
    ax1.plot(m.TIME,U[2]["DX"])
    
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel("displacement [m]")
    ax1.grid()
    # ax1.legend()
    fig1.tight_layout()
    fig1.savefig(u"{}_transient.png".format(Path(__file__).stem))
    
    plt.show()
    
    plt.close()
    