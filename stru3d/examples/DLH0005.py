#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de calcul de la réponse harmonique d'un système "nested LPM"
#
#     ATTENTION: dans les calculs que nous effectuons dans cet exemple, on n'a
#     pas considéré de masse dans le modèle, c'est pour cela la solution du
#     modèle complet et celle du modèle équivalent matchent très bien.
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import dot
from math import pi,cos
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
from stru3d.core.model import model


def tukeywin(time,t1,t2,alpha):
    """
    Fonction qui retourne un array permetant d'appliquer
    une fenêtre de type Tukey sur un array de valeurs
    - in:
        time    - vecteur des instants de temps
        t1      - limite inférieure du plateau
        t2      - limite supérieure du plateau
        alpha   - paramètre controlant le saut de la fenêtre
    - out:
        timetk  - array à appliquer au vecteur de données
    """
    tA = ((alpha-2.)*t1+alpha*t2)/(2.*(alpha-1.))
    tC = t1
    tD = t2
    tF = (alpha*t1+(alpha-2.)*t2)/(2.*(alpha-1.))
    timetk = numpy.zeros(time.shape, dtype=numpy.float64)
    for i in range(0,len(time)):
        t = time[i]
        if tA < t and t < tC:
            timetk[i] = 0.5*(1.+cos(2.*pi*(1-alpha)*(t-t1)/(alpha*(t2-t1))))
        elif tC <= t and t <= tD:
            timetk[i] = 1.
        elif tD < t and t < tF:
            timetk[i] = 0.5*(1.+cos(2.*pi*(1-alpha)*(t-t2)/(alpha*(t2-t1))))
    return timetk



# test
if __name__ == "__main__":
    
    # propriétés du système
    ks1 = 20.
    cs1 = 0.1732
    ms1 = 0.
    ks2 = -10.
    cs2 = -0.6
    ms2 = 0.
    # ms2 = 1.45
    
    lfreq = numpy.linspace(0.5,20.)
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[1.,0.,0.],
            3:[2.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,3],
                2:[1,3],
                4:[2,3],
                5:[1,2],
            },
            "POI1":{
                3:[2],
                6:[3],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx":ks1,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[2],
                "DATA":{
                    "cx":cs1,
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[3],
                "DATA":{
                    "mx":ms1,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[4],
                "DATA":{
                    "kx":ks2,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[5],
                "DATA":{
                    "cx":cs2,
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[6],
                "DATA":{
                    "mx":ms2,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":3,"VALUE":{"FX":1.}},
        ],
        "TYPE_SOL":"DYNA_LINE_HARM",
        "LFREQ":lfreq,
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    U = m.SOLUTION["U"]
    lfreq = m.LFREQ
    limp = 1./U[3]["DX"][:]
    
    
    fig, axarr = plt.subplots(2,2)
    
    axarr[0,0].plot(lfreq,limp.real,"-")
    axarr[0,1].plot(lfreq,limp.imag,"-")
    axarr[1,0].plot(lfreq,numpy.abs(limp),"-")
    axarr[1,1].plot(lfreq,numpy.abs(numpy.angle(limp)),"-")
    
    axarr[0,0].axhline(y=0.,color="k",ls="--")
    axarr[0,0].set_xlabel("freq [Hz]")
    axarr[0,0].set_ylabel(r"$\Re (K)$")
    # axarr[0,0].legend(loc=0)
    
    axarr[0,1].set_xlabel("freq [Hz]")
    axarr[0,1].set_ylabel(r"$\Im (K)$")
    
    axarr[1,0].set_xlabel("freq [Hz]")
    axarr[1,0].set_ylabel("complex modulus")
    
    axarr[1,1].axhline(y=pi/2.,color="k",ls="--")
    axarr[1,1].set_xlabel("freq [Hz]")
    axarr[1,1].set_ylabel("phase angle")
    
    for i in range(0,2):
        for j in range(0,2):
            axarr[i,j].grid()
    
    plt.tight_layout()
    
    fig.savefig(u"{}_impedance.png".format(Path(__file__).stem))
    plt.close()
    
    
    # plot des déplacements
    U2 = U[2]["DX"][:]
    U3 = U[3]["DX"][:]
    
    fig, axarr = plt.subplots(2)
    
    axarr[0].plot(lfreq,U2.real,label="N2")
    axarr[0].plot(lfreq,U3.real,label="N3")
    axarr[1].plot(lfreq,U2.imag)
    axarr[1].plot(lfreq,U3.imag)
    
    axarr[0].set_xlabel("freq [Hz]")
    axarr[0].set_ylabel("disp. real part")
    axarr[0].legend(loc=0,fontsize=10)
    axarr[0].grid()
    axarr[1].set_xlabel("freq [Hz]")
    axarr[1].set_ylabel("disp. imag part")
    axarr[1].grid()
    
    plt.tight_layout()
    
    fig.savefig(u"{}_disp.png".format(Path(__file__).stem))
    plt.close()
    
    
    
    # résolution du problème en temporelle avec une force
    # qui varie avec le temps
    time = numpy.arange(0.,10.,0.005)
    force = numpy.sin(2.*pi*(5.*time/10.+0.1)*time)
    
    t1 = 1.5
    t2 = 6.5
    alpha = 0.35
    tkw = tukeywin(time,t1,t2,alpha)
    
    force2 = tkw*force
    
    # fig,ax = plt.subplots(1)
    
    # ax.plot(time,force)
    # ax.plot(time,tkw)
    # ax.plot(time,force2)
    
    # plt.show()
    # plt.close()
    
    dyna_load ={
        "time":time,
        "force":force2,
    }
    
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[1.,0.,0.],
            3:[2.,0.,0.],
        },
        "MESH":{
            "SEG2":{
                1:[1,3],
                2:[1,3],
                4:[2,3],
                5:[1,2],
            },
            "POI1":{
                3:[2],
                6:[3],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"SPRING",
                "MESH":[1],
                "DATA":{
                    "kx":ks1,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[2],
                "DATA":{
                    "cx":cs1,
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[3],
                "DATA":{
                    "mx":ms1,
                },
            },
            {"ELEM_TYPE":"SPRING",
                "MESH":[4],
                "DATA":{
                    "kx":ks2,
                },
            },
            {"ELEM_TYPE":"DASHPOT",
                "MESH":[5],
                "DATA":{
                    "cx":cs2,
                },
            },
            {"ELEM_TYPE":"MASS",
                "MESH":[6],
                "DATA":{
                    "mx":ms2,
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            {"NODE":3,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":3,"VALUE":{"FX":dyna_load}},
        ],
        "DT":0.005,                     # pas de temps à utiliser dans la résolution
        "TFIN":10.,                     # temps à la fin de la résolution
        "TYPE_SOL":"DYNA_LINE",
    }
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    Uref = m.SOLUTION["U"]
    Vref = m.SOLUTION["V"]
    Aref = m.SOLUTION["A"]
    TIME = m.TIME
    
    
    fig, axarr = plt.subplots(3)
    
    axarr[0].plot(TIME,Uref[2]["DX"][:] ,label="N2")
    axarr[0].plot(TIME,Uref[3]["DX"][:],label="N3")
    axarr[1].plot(TIME,Vref[2]["DX"][:] )
    axarr[1].plot(TIME,Vref[3]["DX"][:])
    axarr[2].plot(TIME,Aref[2]["DX"][:] )
    axarr[2].plot(TIME,Aref[3]["DX"][:])
    
    axarr[0].set_xlabel(u"temps (s)")
    axarr[0].set_ylabel(u"déplacement (m)")
    axarr[0].legend(loc=0,fontsize=10)
    axarr[0].grid()
    axarr[1].set_xlabel(u"temps (s)")
    axarr[1].set_ylabel(u"vitesse (m/s)")
    axarr[1].grid()
    axarr[2].set_xlabel(u"temps (s)")
    axarr[2].set_ylabel(u"accélération (m/s2)")
    axarr[2].grid()
    
    plt.tight_layout()
    
    fig.savefig(u"{}_dyna.png".format(Path(__file__).stem))
    plt.close()
    
    
