#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution statique d'un ressort soumis à des déplacements
# imposés à ses extrémités
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    l_cas_k =[
        {"kx":100.},
        {"ky":100.},
        {"kz":100.},
    ]
    
    # liste avec les solutions en déplacement cas par cas
    l_disp_test =[
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 0., 1., 0.,  0. , 0. ]),
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 0., 1., 0.,  0. , 0. ]),
        numpy.array([ 0., 0., 0., 0., 0., 0., 0., 0., 1., 0.,  0. , 0. ]),
    ]
    
    # idem avec le vecteur de forces
    l_force_test =[
        numpy.array([   0., 0., 0., 0., 0. , 0.,  0., 0., 0., 0., 0., 0.]),
        numpy.array([   0., 0., 0., 0., 0. , 0.,  0., 0., 0., 0., 0., 0.]),
        numpy.array([   0., 0., -100., 0., 0. , 0.,  0., 0., 100., 0., 0., 0.]),
    ]
    
    i = 1
    for cas_k,disp_test,force_test in zip(l_cas_k,l_disp_test,l_force_test):
        
        # dictionnaire qui contient les informations du problème a réssoudre
        model_dict ={
            "NODE":{
                1:[0.,0.,0.],
                2:[1.,0.,0.],
            },
            "MESH":{
                "SEG2":{
                    1:[1,2],
                },
            },
            "ELEM":[
                {"ELEM_TYPE":"SPRING",
                    "MESH":[1],
                    "DATA":cas_k,
                },
            ],
            "NODE_DISP":[
                {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":2,"VALUE":{'DX':0, 'DY':0, 'DZ':1, 'RX':0, 'RY':0, 'RZ':0}},
            ],
            "NODE_LOADS":[],
            "TYPE_SOL":"STAT_LINE",
        }
        
        # résolution du problème
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        # vérification du résultat
        U,F = [m.SOLUTION[t] for t in ['U','F']]
        U = assembly.node2dof(m,U,"disp")
        F = assembly.node2dof(m,F,"force")
        print("cas {}: ".format(i),((numpy.sum(numpy.abs(U-disp_test )) < 1e-10) and \
                                    (numpy.sum(numpy.abs(F-force_test)) < 1e-10)))
        
        i += 1

