#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution dynamique linéaire (Newmark) avec amortissement de
# type Rayleigh donné directement aux éléments. Un calcul comparatif est
# effectué avec la solution avec amortissement de type Rayleigh mais défini
# au niveau global
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    l_cas_damping =[
        # label / damping information for global Rayleigh damping / damping information for elements / plot line options
        [u"0 %",{},{},'-'],
        [u"5 % (global)",{"type":"RAYLEIGH","f1":0.3,"f2":1.,"xi1":0.05,"xi2":0.05},{},"-"],
        [u"5 % (elems)",{},{"type":"RAYLEIGH","f1":0.3,"f2":1.,"xi1":0.05,"xi2":0.05},"--"],
    ]
    
    dyna_load ={
        "time":[1.,1.2,1.4,1.6,1.8,2.],
        "force":[0.,0.6,1.,1.,0.6,0.],
    }
    
    
    fig_disp, ax_disp = plt.subplots(1)
    fig_vit, ax_vit = plt.subplots(1)
    fig_acc, ax_acc = plt.subplots(1)
    
    for label,d_damp_glob,d_damp_elem,line_style in l_cas_damping:
        
        # dictionnaire qui contient les informations du problème a réssoudre
        model_dict ={
            "NODE":{
                1:[0.,0.,0.],
                2:[1.,0.,0.],
            },
            "MESH":{
                "SEG2":{
                    1:[1,2],
                },
                "POI1":{
                    2:[2],
                },
            },
            "ELEM":[
                {"ELEM_TYPE":"SPRING",
                    "MESH":[1],
                    "DATA":{
                        "kx":1.,
                        "damping":d_damp_elem,
                    },
                },
                {"ELEM_TYPE":"MASS",
                    "MESH":[2],
                    "DATA":{
                        "mx":0.159,
                        "damping":d_damp_elem,
                    },
                },
            ],
            "NODE_DISP":[
                {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            ],
            "NODE_LOADS":[
                {"NODE":2,"VALUE":{"FX":dyna_load}},
            ],
            "DT":0.05,                      # pas de temps à utiliser dans la résolution
            "TFIN":10.,                     # temps à la fin de la résolution
            "DAMPING":d_damp_glob,
            "TYPE_SOL":"DYNA_LINE",
        }
        
        
        # résolution du problème
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
        V = assembly.node2dof(m,m.SOLUTION["V"],"disp")
        A = assembly.node2dof(m,m.SOLUTION["A"],"disp")
        TIME = m.TIME
        
        ax_disp.plot(TIME,U[6,:],line_style,label=label)
        ax_vit.plot(TIME,V[6,:],line_style,label=label)
        ax_acc.plot(TIME,A[6,:],line_style,label=label)
        
        
    ax_disp.set_xlabel(u"Temps (s)")
    ax_disp.set_ylabel(u"Déplacement (m)")
    ax_disp.legend(loc=0)
    ax_disp.grid()
    fig_disp.savefig(u"{}_disp.png".format(Path(__file__).stem))
    
    ax_vit.set_xlabel(u"Temps (s)")
    ax_vit.set_ylabel(u"Vitesse (m/s)")
    ax_vit.legend(loc=0)
    ax_vit.grid()
    fig_vit.savefig(u"{}_vit.png".format(Path(__file__).stem))
    
    ax_acc.set_xlabel(u"Temps (s)")
    ax_acc.set_ylabel(u"Accélération (m/s2)")
    ax_acc.legend(loc=0)
    ax_acc.grid()
    fig_acc.savefig(u"{}_acc.png".format(Path(__file__).stem))
    
    plt.close()
    
    