#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de comparaison des algorithmes de résolution dynamique linéaire et
# non-linéaire. On calcule un problème élastique linéaire et on compare le
# résultat
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
from stru3d.core.model import model
from stru3d.core.assembly import assembly


# test
if __name__ == "__main__":
    
    
    dyna_load ={
        "time":[1.,1.2,1.4,1.6,1.8,2.],
        "force":[0.,0.6,1.,1.,0.6,0.],
    }
    
    fig_disp, ax_disp = plt.subplots(1)
    fig_vit, ax_vit = plt.subplots(1)
    fig_acc, ax_acc = plt.subplots(1)
    for t in ["DYNA_LINE","DYNA_NON_LINE"]:
        
        # dictionnaire qui contient les informations du problème a réssoudre
        model_dict ={
            "NODE":{
                1:[0.,0.,0.],
                2:[1.,0.,0.],
            },
            "MESH":{
                "SEG2":{
                    1:[1,2],
                },
                "POI1":{
                    2:[2],
                },
            },
            "ELEM":[
                {"ELEM_TYPE":"SPRING",
                    "MESH":[1],
                    "DATA":{
                        "kx":1.,
                    },
                },
                {"ELEM_TYPE":"MASS",
                    "MESH":[2],
                    "DATA":{
                        "mx":0.159,
                    },
                },
            ],
            "NODE_DISP":[
                {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
                {"NODE":2,"VALUE":{        'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
            ],
            "NODE_LOADS":[
                {"NODE":2,"VALUE":{"FX":dyna_load}},
            ],
            "DT":0.05,              # pas de temps à utiliser dans la résolution
            "TFIN":10.,             # temps à la fin de la résolution
            "TOL":1e-10,            # tolerance critère convergence Newton-Raphson modifié
            "SCHEME":"mNR",         # mNR = modified Newton-Raphson
            "TYPE_SOL":t,
        }
        
        
        # résolution du problème
        m = model(model_dict=model_dict)
        m.process_DOF()
        m.solve()
        
        U = assembly.node2dof(m,m.SOLUTION["U"],"disp")
        V = assembly.node2dof(m,m.SOLUTION["V"],"disp")
        A = assembly.node2dof(m,m.SOLUTION["A"],"disp")
        TIME = m.TIME
        
        
        ax_disp.plot(TIME,U[6,:],"-",label=t)
        ax_vit.plot(TIME,V[6,:],"-",label=t)
        ax_acc.plot(TIME,A[6,:],"-",label=t)
    
    ax_disp.set_xlabel("Temps (s)")
    ax_disp.set_ylabel("Displacement (m)")
    ax_disp.legend(loc=0)
    ax_disp.grid()
    ax_vit.set_xlabel("Temps (s)")
    ax_vit.set_ylabel("Vitesse (m/s)")
    ax_vit.legend(loc=0)
    ax_vit.grid()
    ax_acc.set_xlabel("Temps (s)")
    ax_acc.set_ylabel("Accélération (m/s^2)")
    ax_acc.legend(loc=0)
    ax_acc.grid()
    
    fig_disp.savefig(u"{}_disp.png".format(Path(__file__).stem))
    fig_vit.savefig(u"{}_vit.png".format(Path(__file__).stem))
    fig_acc.savefig(u"{}_acc.png".format(Path(__file__).stem))
    
    plt.close()
    
    