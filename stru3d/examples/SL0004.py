#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Exemple de résolution statique d'un système avec deux poutres élastiques
# linéaires
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   06/08/2022
#-----------------------------------------------------------------------------#

import numpy
from math import pi
from stru3d.core.model import model
from stru3d.core.assembly import assembly

# test
if __name__ == "__main__":
    
    U_test = [ 0., 0., 0., 0., 0., 0., 5., 0., 0., 0., 9., 0., 16., 0., 0., 0., 12., 0.]
    
    # dictionnaire qui contient les informations du problème a réssoudre
    model_dict ={
        "NODE":{
            1:[0.,0.,0.],
            2:[0.,0.,1.],
            3:[0.,0.,2.],
        },
        "MESH":{
            "SEG2":{
                1:[1,2],
                2:[2,3],
            },
        },
        "ELEM":[
            {"ELEM_TYPE":"BEAM",
                "MESH":[1],
                "DATA":{
                    "geometry":{
                        "section": "rectangle",
                        "a":0.1,
                        "b":0.1,
                        "v":(0.,1.,0.),
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.2,
                    },
                },
            },
            {"ELEM_TYPE":"BEAM",
                "MESH":[2],
                "DATA":{
                    "geometry":{
                        "section": "rectangle",
                        "a":0.1,
                        "b":0.1,
                        "v":(0.,1.,0.),
                    },
                    "material":{
                        "E":20000.,
                        "nu":0.2,
                    },
                },
            },
        ],
        "NODE_DISP":[
            {"NODE":1,"VALUE":{'DX':0, 'DY':0, 'DZ':0, 'RX':0, 'RY':0, 'RZ':0}},
        ],
        "NODE_LOADS":[
            {"NODE":3,"VALUE":{"FX":1.}},
        ],
        "TYPE_SOL":"STAT_LINE",
    }
    
    
    # résolution du problème
    m = model(model_dict=model_dict)
    m.process_DOF()
    m.solve()
    
    
    # vérification du résultat
    U,F = [m.SOLUTION[t] for t in ['U','F']]
    U = assembly.node2dof(m,U,"disp")
    F = assembly.node2dof(m,F,"force")
    
    print("cas: {}:".format(1), (max(numpy.abs(numpy.array(U) - numpy.array(U_test))) < 1e-10))
    
