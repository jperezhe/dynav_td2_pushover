#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Librairie qui contient les opérations géométriques qui peuvent être
# effectuées sur les résultats, les éléments, etc.
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from math import sqrt

class geom():
    """
    Module qui regroupe toutes les opérations géométriques (obtention de la
    matrice de rotation, etc.)
    """
    
    @staticmethod
    def rotation_matrix_seg(p1,p2,v):
        """
        Fonction qui permet calculer la matrice de rotation qui transforme
        un vecteur donné dans le système de référence global au système local
        pour le cas d'un segment
        - in:
            p1,p2       - triplets de coordonées des noeuds du segment
            v           - vecteur qui définit le vecteur y' orthogonal à la
                          directrice du segment (x'), par proyection dans le
                          plan orthogonal à la directrice
        - out:
            R           - matrice de rotation (12x12)
        NOTA: consulter NOTES/geometry/rotation_matrix.ipynb pour plus de détails
        """
        (x1,y1,z1) = p1
        (x2,y2,z2) = p2
        # Directrice du segment: x' = (a1,a2,a3)
        a1 = (x2-x1)/sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2)
        a2 = (y2-y1)/sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2)
        a3 = (z2-z1)/sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2)
        
        # Verseur y' = (b1,b2,b3)
        (v1,v2,v3) = v
        v1_ = v1 - (v1*a1+v2*a2+v3*a3)*a1
        v2_ = v2 - (v1*a1+v2*a2+v3*a3)*a2
        v3_ = v3 - (v1*a1+v2*a2+v3*a3)*a3
        b1 = v1_/sqrt(v1_**2+v2_**2+v3_**2)
        b2 = v2_/sqrt(v1_**2+v2_**2+v3_**2)
        b3 = v3_/sqrt(v1_**2+v2_**2+v3_**2)
        
        # Verseur z' = (c1,c2,c3)
        c1 = a2*b3 - a3*b2
        c2 = a3*b1 - a1*b3
        c3 = a1*b2 - a2*b1
        
        r = numpy.array([[a1,a2,a3],[b1,b2,b3],[c1,c2,c3]])
        O = numpy.zeros((3,3),dtype=numpy.float)
        R1 = numpy.hstack((r,O,O,O))
        R2 = numpy.hstack((O,r,O,O))
        R3 = numpy.hstack((O,O,r,O))
        R4 = numpy.hstack((O,O,O,r))
        
        return numpy.vstack((R1,R2,R3,R4))
    
    @staticmethod
    def rotation_matrix_axis(u,v,ndof=12):
        """
        Fonction qui permet calculer la matrice de rotation qui transforme
        un vecteur donné dans le système de référence global au système local
        donné par deux de ses vecteurs directeurs
        - in:
            u,v     - vecteurs directeurs
          NOTA: le vecteur v est projecté sistematiquement dans le plan orthogonal
          au vecteur u. Il est donc possible de fournir un vecteur "équivalent"
          (dont la projection dans le plan orthogonal est la même)
          NOTA2: le troisième vecteur directeur du repère locale est obtenu de
          manière automatique par multiplication vectorielle à partir des deux
          autres vecteurs
        - out:
            R       - matrice de rotation (12x12)
        """
        (u1,u2,u3) = u
        (v1,v2,v3) = v
        
        # Verseur x' = (a1,a2,a3)
        a1 = u1/sqrt(u1**2+u2**2+u3**2)
        a2 = u2/sqrt(u1**2+u2**2+u3**2)
        a3 = u3/sqrt(u1**2+u2**2+u3**2)
        
        # Verseur y' = (b1,b2,b3)
        v1_ = v1 - (v1*a1+v2*a2+v3*a3)*a1
        v2_ = v2 - (v1*a1+v2*a2+v3*a3)*a2
        v3_ = v3 - (v1*a1+v2*a2+v3*a3)*a3
        b1 = v1_/sqrt(v1_**2+v2_**2+v3_**2)
        b2 = v2_/sqrt(v1_**2+v2_**2+v3_**2)
        b3 = v3_/sqrt(v1_**2+v2_**2+v3_**2)
        
        # Verseur z' = (c1,c2,c3)
        c1 = a2*b3 - a3*b2
        c2 = a3*b1 - a1*b3
        c3 = a1*b2 - a2*b1
        
        r = numpy.array([[a1,a2,a3],[b1,b2,b3],[c1,c2,c3]])
        O = numpy.zeros((3,3),dtype=numpy.float)
        if ndof == 3:
            return r
        elif ndof == 6:
            R1 = numpy.hstack((r,O))
            R2 = numpy.hstack((O,r))
            return numpy.vstack((R1,R2))
        elif ndof == 12:
            R1 = numpy.hstack((r,O,O,O))
            R2 = numpy.hstack((O,r,O,O))
            R3 = numpy.hstack((O,O,r,O))
            R4 = numpy.hstack((O,O,O,r))
            return numpy.vstack((R1,R2,R3,R4))
        else:
            return False
    
    
    @staticmethod
    def length(n1,n2):
        """
        Calcul de la longueur d'un segment
        """
        (x1,y1,z1) = n1
        (x2,y2,z2) = n2
        return sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2)
    
