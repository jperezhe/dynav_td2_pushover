#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Librairie qui contient les fonctionnalitées nécessaires à tracer les
# résultats d'un calcul et/ou un modèle
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from math import sqrt
from stru3d.elements.beam import beam

class plot():
    """
    Module qui regroupe toutes les opérations graphiques
    """
    
    @staticmethod
    def model(model=None,axis=None,style={"color":"grey","ls":"-","marker":"o","markersize":2}):
        """
        Permet de tracer le modèle
        
        NOTA IMPORTANTE: cette version ne permet que traiter que les
        coordonées Y et Z d'un modèle donné. Il faudra rendre le script
        plus polyvalente par la suite
        """
        for elem in model.ELEM:
            if isinstance (elem,(beam)):
                x = []
                y = []
                for no in elem.nodes:
                    x.append(model.NODE[no][1])
                    y.append(model.NODE[no][2])
                axis.plot(x,y,**style)
    
    @staticmethod
    def mode(model=None,axis=None,mode=None,factor=1.,
             style={"color":"k","ls":"-","marker":"o","markersize":2}):
        """
        Permet de tracer un mode de réponse donné
        """
        for elem in model.ELEM:
            if isinstance (elem,(beam)):
                x = []
                y = []
                for no in elem.nodes:
                    x.append(model.NODE[no][1] + model.SOLUTION["modes"][no]["DY"][mode-1]*factor)
                    y.append(model.NODE[no][2] + model.SOLUTION["modes"][no]["DZ"][mode-1]*factor)
                axis.plot(x,y,**style)
        
