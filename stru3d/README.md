# Stru3D

**Logiciel généraliste de calcul aux éléments finis codé en Python.**

* Auteur: Jesús Pérez <jesus.perez@setec.com>

## Installation

### Option 1 (compatible avec la distribution Anaconda)

Ajouter un fichier `*.pth` dans `C:\ProgramData\Anaconda3\Lib\site-packages` (ou équivalent), et indiquer à l'intérieur le path où se trouve la librairie. Par exemple:

```
# .pth file for the stru3d library
C:\DATA\DOSSIERS\Stru3D
```

Maintenant lorsque nous sommes dans un interprète Python, on peut vérifier que le dossier est bien pris en compte avec:

```python
import sys
sys.path
```

Qui nous retourne la liste des dossiers dans lesquels l'interprète Python ira chercher les différents modules.

### Option 2 (compatible avec les distributions basiques de Python)

L'adresse du repertoire contenant la librairie doit être ajoutée aux
variables d'environnement PYTHONPATH et PATH du système d'exploitation. Pour
ce faire: `Ordinateur > Propriétés système > Paramètres système avancés > Variables d'environnement`

## Eléments disponibles dans la version actuelle du logiciel

### Algorithmes de résolution

- `DYNA_HARM_MODAL`: Réponse dynamique sous chargement harmonique par superposition modale
- `DYNA_LINE`: Réponse dynamique transitoire en linéaire
- `DYNA_LINE_HARM`: Réponse sous chargement harmonique
- `DYNA_NON_LINE`: Réponse dynamique non-linéaire avec Newton-Raphson et Newmark
- `DYNA_TRAN_MODAL`: Réponse dynamique transitoire par superposition modale
- `MODES_LINE`: Résolution modale
- `STAT_LINE`: Résolution problème statique linéaire
- `STAT_NON_LINE`: Résolution problème statique non-linéaire

### Eléments finis

- `BEAM`: Poutre droite de type Euler-Bernoulli
- `DASHPOT`: Amortisseur visqueux
- `GYROMASS`: Elément discret de type *gyro-mass* d'après Saitoh (2007)
- `MASS`: Masse ponctuelle
- `PILE_HEAD`: Matrice d'impédance 6x6 en tête d'un pieu isolé à partir des formules analytiques de dans EC8-5 et Gazetas (1991)
- `SPRING`: Réssort linéaire élastique

### Lois de comportement

A venir.


