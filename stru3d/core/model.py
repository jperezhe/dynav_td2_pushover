#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Module permettant la création d'un objet de type model et l'initialisation
# du modèle de calcul
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import collections
import numpy
from numpy import array, zeros, dot
from math import sin, cos, pi, sqrt
import pickle
from stru3d.core.assembly import assembly
from stru3d.core.support import aslist,interp_data,pmsg
from stru3d.core.io import mail
from stru3d.elements.cata import _d_elem,l_elas_elem,l_non_elas_elem
from stru3d.solvers.cata import CATA_SOLVERS,l_elas_solve,l_non_elas_solve

class model():
    
    def __init__(self, route_model_inp=None, model_dict=None):
        """
        Initialisation d'une instance de type model qui contiendra
        toutes les informations relatives au modèle de calcul ainsi
        que les résultats de calcul
        """
        self.NODE = collections.OrderedDict()
        self.MESH = collections.OrderedDict()
        self.MESH_TYPE = collections.OrderedDict()
        self.GROUP_MA = collections.OrderedDict()
        self.GROUP_NO = collections.OrderedDict()
        
        self.ELEM = []
        self.NODE_LOADS = collections.OrderedDict()
        self.NODE_DISP = collections.OrderedDict()
        self.BLOQ_DDL_NODE = collections.OrderedDict()
        self.EARTHQUAKE = None      # chargement sismique
        self.GRAVITY = None         # chargement poids propre
        self.DAMPING = None         # options pour calcul amortissement
        
        self.DOF = collections.OrderedDict()
        self.DOF['NODE'] = collections.OrderedDict()
        self.DOF['NUM']  = []
        self.DOF['BLOQ'] = []
        self.DOF['FREE'] = []
        self.DOF['LAG_MULT'] = []   # liste des multiplicateurs de Lagrange
        
        self.TYPE_SOL = ""          # type de problème à réssoudre
                                    # STAT_LINE, STAT_NON_LINE, MODES_LINE,
                                    # DYNA_LINE, DYNA_LINE_HARM,
                                    # DYNA_NON_LINE
        self.SOLUTION = collections.OrderedDict()
        
        self.NSTEP = 1              # nombre de pas dans la résolution d'un problème non-linéaire
        self.TOL = 1e-20            # tolerance à utiliser dans le critère de convergence
                                    # d'un algorithme de résolution non-linéaire
        self.SCHEME = "mNR"         # schema de résolution problème non-linéaire
                                    #   mNR - modified Newton-Raphson
                                    #   NR  - Newton-Raphson
        self.NITER_MAX = 20         # nombre d'itérations maximale à effectuer dans les
                                    # schemas de résolution non-linéaires
        
        self.SEL_MODES = None       # liste avec les modes à prendre en compte dans un calcul
                                    # transitoire par superposition modale
        
        self.CHECK_INPUT = True     # checks input data before analysis
        self.INFO = True            # prints information about resolution
        self.WARNING_INFO = True    # prints warning information to avoid wrong misuse of code
        self.ERROR_INFO = True      # prints information about errors in input data, etc.
        self.FORCE_RES = False      # when True solution the resolution is continuated even if the
                                    # max number of iterations has been reached
        
        if route_model_inp:
            pass
        elif model_dict:
            self.read_model_dict(model_dict=model_dict)
        
        
    
    def read_model_inp(self, route_model_inp=None):
        """
        Fonction de lecture d'un fichier contenant le modèle à résoudre
        """
        
        source = open(route,'r')
        a = source.readlines()
        source.close()
        
        # Nettoyage du fichier de données (commentaires, etc.)
        for i in range(0,len(a)):
            a[i] = a[i].split()
        
        k = 0
        for i in range(0,len(a)):
            if '#' in a[i-k]:
                del a[i-k]
                k += 1
        
        # Lecture ligne par ligne du fichier de données
        for i in range(0,len(a)):
            # information sur le maillage
            if '*NODES' in a[i]:
                k = 1
                while (a[i+k] != ['*FIN']):
                    no = int(a[i+k][0])
                    x  = float(a[i+k][1])
                    y  = float(a[i+k][2])
                    z  = float(a[i+k][3])
                    self.NODE[no] = [x, y, z]
                    k += 1
                    if i+k == len(a):
                        break
            if '*SEG' in a[i]:
                k = 1
                while (a[i+k] != ['*FIN']):
                    ma  = int(a[i+k][0])
                    no1 = int(a[i+k][1])
                    no2 = int(a[i+k][2])
                    
                    # TODO: continue avec codage de cette fonctionnalité
                
    
    def read_model_dict(self, model_dict=None):
        """
        Permet la lecture des données du modèle à partir d'un
        dictionnaire
        """
        if not model_dict:
            return
        
        # Lecture du maillage à partir d'un fichier de maillage Code_Aster
        # NOTA IMPORTANTE: ce maillage est ensuite surchargée par les instructions
        # classiques données directement dans le script de calcul (cela permet
        # entre autres d'ajouter des nouveaux éléments du maillage, maillage non
        # conforme avec le standard "mail", etc.
        if "MAIL" in model_dict.keys():
            # le maillage est lu à partir d'un fichier de maillage Code_Aster
            mesh = mail(route_mail=model_dict["MAIL"])
            self.NODE = mesh.NODE
            self.MESH = mesh.MESH
            self.MESH_TYPE = mesh.MESH_TYPE
            self.GROUP_MA = mesh.GROUP_MA
            self.GROUP_NO = mesh.GROUP_NO
        
        # Lecture des instructions concernant le maillage données dans le code de calcul
        
        # lecture des noeuds
        if "NODE" in model_dict.keys():
            for no in sorted(model_dict["NODE"].keys()):
                self.NODE[no] = model_dict["NODE"][no]
        
        # lecture des mailles
        _mesh_type =['POI1','SEG2','NPOINTS']
        if "MESH" in model_dict.keys():
            for mt in _mesh_type:
                if mt in model_dict["MESH"].keys():
                    for ma in sorted(model_dict["MESH"][mt].keys()):
                        self.MESH[ma] = model_dict["MESH"][mt][ma]
                        self.MESH_TYPE[ma] = mt
        
        # lecture des groupes de mailles
        if "GROUP_MA" in model_dict.keys():
            self.GROUP_MA = model_dict["GROUP_MA"]
        
        # lecture des groupes de noeuds
        if "GROUP_NO" in model_dict.keys():
            self.GROUP_NO = model_dict["GROUP_NO"]
        
        # lecture des informations sur les éléments
        for elem in model_dict["ELEM"]:
            elem_type = elem["ELEM_TYPE"]
            
            if elem_type in _d_elem.keys():
                if "GROUP_MA" in elem.keys():
                    gma = elem["GROUP_MA"]
                    lma = aslist(self.GROUP_MA[gma])
                else:
                    lma = aslist(elem["MESH"])
                for ma in lma:
                    nodes = self.MESH[ma]
                    dcoords ={}
                    for no in nodes:
                        dcoords[no] = self.NODE[no]
                    self.ELEM.append(_d_elem[elem_type](nodes=nodes,dcoords=dcoords,
                                            **elem["DATA"]))
            
            else:
                print("Error: element '{}' is not available!".format(elem_type))
        
        # lecture des conditions aux limites et déplacements aux noeuds
        if "NODE_DISP" in model_dict.keys():
            for disp in model_dict["NODE_DISP"]:
                lno = []
                if "GROUP_NO" in disp.keys():
                    gno = disp["GROUP_NO"]
                    lno = self.GROUP_NO[gno]
                elif "NODE" in disp.keys():
                    lno = aslist(disp["NODE"])
                else:
                    print("Error (NODE_DISP): there is an error in given commands")
                
                for no in sorted(lno):
                    # TODO: changer la ligne suivante pour permettre la possibilité
                    # de définir le déplacement aux différents ddl de chaque noeud
                    # de manière indépendante...
                    self.NODE_DISP[no] = disp["VALUE"].copy()   # copy needed to avoid modification of data dict
        
        # lecture des chargements
        if "NODE_LOADS" in model_dict.keys():
            for nload in model_dict["NODE_LOADS"]:
                lno = []
                if "GROUP_NO" in nload.keys():
                    gno = nload["GROUP_NO"]
                    lno = self.GROUP_NO[gno]
                elif "NODE" in nload.keys():
                    lno = aslist(nload["NODE"])
                else:
                    print("Error (NODE_LOADS): there is an error in given commands")
                
                for no in sorted(lno):
                    # TODO: idem, même commentaire que pour NODE_DISP
                    self.NODE_LOADS[no] = nload["VALUE"].copy()     # copy needed to avoid modification of data dict
        
        if "NSTEP" in model_dict.keys():
            self.NSTEP = int(model_dict["NSTEP"])
        
        if "TOL" in model_dict.keys():
            self.TOL = model_dict["TOL"]
        
        if "NITER_MAX" in model_dict.keys():
            self.NITER_MAX = int(model_dict["NITER_MAX"])
        
        if "FORCE_RES" in model_dict.keys():
            self.FORCE_RES = model_dict["FORCE_RES"]
        
        if "SCHEME" in model_dict.keys():
            self.SCHEME = model_dict["SCHEME"]
        
        if "CHECK_INPUT" in model_dict.keys():
            self.CHECK_INPUT = model_dict["CHECK_INPUT"]
        
        if "INFO" in model_dict.keys():
            self.INFO = model_dict["INFO"]
        
        if "WARNING_INFO" in model_dict.keys():
            self.WARNING_INFO = model_dict["WARNING_INFO"]
        
        if "ERROR_INFO" in model_dict.keys():
            self.ERROR_INFO = model_dict["ERROR_INFO"]
        
        # lecture des informations sur le type de résolution
        if "TYPE_SOL" in model_dict.keys():
            self.TYPE_SOL = model_dict["TYPE_SOL"]
            
            if model_dict["TYPE_SOL"] in ["DYNA_LINE","DYNA_TRAN_MODAL","DYNA_NON_LINE"]:
                
                self.DT = model_dict["DT"]
                self.TFIN = model_dict["TFIN"]
                # NOTA: pour utiliser l'algorithme de résolution dynamique linéaire et
                # non-linéaire il faut avoir le même DT pour tous les pas de temps
                self.TIME = numpy.arange(0.,self.TFIN+1e-10,self.DT)
                
                # TODO: ajouter ici le code pour traiter le chargement et
                # interpoler le signal du chargement pour chaque node au
                # lieu de le faire directement à l'interieur de l'algorithme
                # de resolution
                
                # chargement sismique
                if "EARTHQUAKE" in model_dict.keys():
                    self.EARTHQUAKE = model_dict["EARTHQUAKE"]
            
            elif model_dict["TYPE_SOL"] in ["STAT_NON_LINE"]:
                
                if ("DT" in model_dict.keys()) and \
                   ("TFIN" in model_dict.keys()):
                    self.DT = model_dict["DT"]
                    self.TFIN = model_dict["TFIN"]
                    self.TIME = numpy.arange(0.,self.TFIN+1e-10,self.DT)
                    self.NSTEP = len(self.TIME)-1
                    
                    for no in self.NODE_LOADS.keys():
                        for t in self.NODE_LOADS[no].keys():
                            self.NODE_LOADS[no][t] = interp_data(self.NODE_LOADS[no][t]["time"],
                                                        self.NODE_LOADS[no][t]["force"],self.TIME)
                    
                    # TODO: ajouter code pour définir chargement en déplacement de la
                    # même manière qui est fait pour les forces ci-dessus
                
                else:
                    
                    self.TIME = numpy.linspace(0.,1.,self.NSTEP+1)
                    
                    for no in self.NODE_LOADS.keys():
                        for t in self.NODE_LOADS[no].keys():
                            self.NODE_LOADS[no][t] = numpy.linspace(0.,self.NODE_LOADS[no][t],self.NSTEP+1)
                    
                    for no in self.NODE_DISP.keys():
                        for t in self.NODE_DISP[no].keys():
                            self.NODE_DISP[no][t] = numpy.linspace(0.,self.NODE_DISP[no][t],self.NSTEP+1)
            
            elif model_dict["TYPE_SOL"] in ["DYNA_LINE_HARM"]:
                self.LFREQ = model_dict["LFREQ"]
            
            elif model_dict["TYPE_SOL"] in ["MODES_LINE"]:
                if "MODE_NORM" in model_dict.keys():
                    self.MODE_NORM = model_dict["MODE_NORM"]
                else:
                    self.MODE_NORM = "MASS"
            
        
        if "GRAVITY" in model_dict.keys():
            self.GRAVITY = model_dict["GRAVITY"]
        
        if "DAMPING" in model_dict.keys():
            self.DAMPING = model_dict["DAMPING"]
    
    
    
    def process_DOF(self):
        """
        Fonction qui permet de numéroter de manière automatique et séquentielle
        tous les degrés de liberté du sytème pour faciliter les opérations
        matricielles ultérieures
        """
        
        # TODO: attention, ceci n'est pas totalement comme ça lors d'un calcul
        # dynamique pour lequel on peut introduire des conditions initiales
        # (par exemple en déplacement, vitesse...) sans pour autant bloquer
        # le ddl correspondant
        
        # on utilise uniquement les noeuds qui sont effectivement utilisés par
        # les éléments du modèle
        nodes = []
        for elem in self.ELEM:
            nodes += elem.nodes
        nodes = sorted(set(nodes))
        
        n_nodes = len(nodes)
        l_num_dof  = set(range(0,n_nodes*6))
        l_bloq_dof = []
        l_free_dof = []
        
        i = 0
        for no in nodes:
            # Chaque degrée de liberté des noeuds du modèle reçoit un numéro
            self.DOF['NODE'][no] = range(i,i+6)
            
            # Recherche des degrés de liberté bloqués
            if no in self.BLOQ_DDL_NODE.keys():
                [ux,uy,uz,rx,ry,rz] = self.BLOQ_DDL_NODE[no]
                if ux == 1:
                    l_bloq_dof.append(i+0)
                if uy == 1:
                    l_bloq_dof.append(i+1)
                if uz == 1:
                    l_bloq_dof.append(i+2)
                if rx == 1:
                    l_bloq_dof.append(i+3)
                if ry == 1:
                    l_bloq_dof.append(i+4)
                if rz == 1:
                    l_bloq_dof.append(i+5)
            i += 6
        
        # Les DDL avec déplacements imposés sont considérés comme des DDL bloqués
        for no in self.NODE_DISP.keys():
            [i0,i1,i2,i3,i4,i5] = self.DOF['NODE'][no]
            dIndex = {'DX':i0, 'DY':i1, 'DZ':i2, 'RX':i3, 'RY':i4, 'RZ':i5}
            for t in self.NODE_DISP[no].keys():
                l_bloq_dof.append(dIndex[t])
        
        l_free_dof = sorted(l_num_dof.difference(l_bloq_dof))
        
        self.DOF['NUM']  = sorted(l_num_dof)
        self.DOF['BLOQ'] = sorted(l_bloq_dof)       # il est important que la liste
                                                    # des ddl soit bien ordonée pour
                                                    # l'assemblage de vecteurs
        self.DOF['FREE'] = sorted(l_free_dof)
    
    
    def before_analysis_check(self):
        """
        Checks that the elements that are declared and the resolution algorithm
        are compatible, i.e., that non-linear resolution scheme is chosen to solve
        a problem with non-linear elements, etc.
        
        TODO: check if the selected resolution algorithm is able to handle every
        element that has been declared in input data
        """
        
        if self.CHECK_INPUT:
            
            # check if the resolution scheme is able to
            # handle all the types of elements that are used
            if self.TYPE_SOL in l_elas_solve:
                nl_elem_flag = False
                for elem in self.ELEM:
                    if isinstance(elem,tuple(l_non_elas_elem)):
                        nl_elem_flag = True
                
                if nl_elem_flag:
                    msg = "WARNING (before_analysis_check): the resolution scheme is linear " +\
                          "but some non-linear elements are being used!"
                    pmsg(msg)
        
    
    def solve(self):
        """
        Résolution du système
        """
        
        # Check of input data before analysis
        self.before_analysis_check()
        
        if self.TYPE_SOL in CATA_SOLVERS.keys():
            CATA_SOLVERS[self.TYPE_SOL](self)
        
        else:
            pmsg("Error: no valid type of resolution is given")
    
    
    def save_data(self,route):
        """
        Fonction qui permet sauvegarder les données d'un modèle dans un
        fichier binaire pour effectuer le post-traitement après le calcul
        """
        with open(route,"wb") as output_file:
            pickle.dump(self, output_file, -1)     # use the highest protocol available
    
    
    def get_system_matrices(self):
        """
        Calcul des matrices du système (raideur, amortissement et masse)
        """
        self.K = assembly.get_K(self)
        self.M = assembly.get_M(self)
        self.C = assembly.get_C(self)


