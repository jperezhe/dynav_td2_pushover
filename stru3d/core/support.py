#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Fonctions de support
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import os
import scipy
import scipy.interpolate

def aslist(elem,sorted_list=False):
    """Retourne elem sous forme d'une liste : si elem est déjà une liste on
    le laisse tel quel, sinon on le met dans une liste."""
    if isinstance(elem,list):
        return elem
    elif isinstance(elem,set):
        if sorted_list:
            try:
                return sorted(elem,key=lambda n: int(n[1:]))
            except:
                return sorted(elem)
        else:
            return list(elem)
    else:
        return [elem]


def pmsg(msg, ncol=80, align='left', lines=False):
    """
    Impression d'un message avec une longueur limitée au nombre
    de colonnes indiquées par 'ncol'
    """
    ls1 = msg.split('\n')
    ls2 = []
    for i in range(0,len(ls1)):
        if len(ls1[i]) == 0:
            ls2.append('')
        elif len(ls1[i]) <= ncol:
            ls2.append(ls1[i])
        else:
            while len(ls1[i]) > ncol:
                k = ncol - 1
                while True:
                    if k == 0:
                        ls2.append(ls1[i][:ncol])
                        ls1[i] = ls1[i][ncol:]
                        break
                    elif ls1[i][k] == ' ':
                        ls2.append(ls1[i][:k])
                        ls1[i] = ls1[i][k+1:]
                        break
                    k -= 1
            if len(ls1) > 0:
                ls2.append(ls1[i][:])
    if lines:
        ls2 = ["-"*ncol] + ls2 + ["-"*ncol]
    if align == 'center':
        for i in range(0,len(ls2)):
            ls2[i] = ls2[i].lstrip(' ')
            ls2[i] = ls2[i].rstrip(' ')
            fs = "{0: ^%s}"%ncol
            ls2[i] = fs.format(ls2[i])
    elif align == 'right':
        for i in range(0,len(ls2)):
            ls2[i] = ls2[i].rstrip(' ')
            fs = "{0: >%s}"%ncol
            ls2[i] = fs.format(ls2[i])
    for l in ls2:
        print("{0}".format(l))

def mkdir(dir):
    """
    This function checks if the directory exists, and if not
    creates it. Necessary to avoid OSError!
    """
    if len(dir) > 0:
        if not os.path.exists(dir):
            os.makedirs(dir)

def split_path(path):
    """
    Returns the folder, the file name and the extension in
    separated strings
    """
    dir,file = os.path.split(path)
    name,ext = os.path.splitext(file)
    return dir,name,ext

def interp_data(x,y,time):
    """
    Permet interpoler le vecteur de chargement pour avoir le chargement
    aux instants de temps indiqués par time
    """
    inter_func = scipy.interpolate.InterpolatedUnivariateSpline(x,y,k=1,ext="const")
    return inter_func(time)


