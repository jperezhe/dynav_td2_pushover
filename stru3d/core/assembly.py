#!/usr/bin/python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------#
# Fonctions por le calcul et l'assemblage des matrices du système
#
#   Auteur: J. Pérez <jesus.perez@setec.com>
#   Date:   04/08/2022
#-----------------------------------------------------------------------------#

import numpy
from numpy import array, zeros, dot
from math import sin, cos, pi, sqrt
import collections
from stru3d.core.support import pmsg
from stru3d.elements.cata import CATA_ELEM_STIFF,CATA_ELEM_MASS,CATA_ELEM_DAMP

class assembly():
    """
    Module qui regroupe toutes les opérations concernant le calcul et assemblage
    des matrices du système
    """
    
    @staticmethod
    def matrix_assembly(Ma,Mb,lai,laj=None):
        """
        Assembly function of two matrix. Ma matrix is supposed to be
        a large matrix where we need to include values from Mb matrix.
        'la' is the list of index of destination cells in Ma matrix (they
        need to be in the same order that was used to get Mb matrix!)
        """
        if not laj:
            Ma[numpy.ix_(lai,lai)] += Mb
        else:
            Ma[numpy.ix_(lai,laj)] += Mb
        return Ma
    
    
    @staticmethod
    def dof2node(model,res,type):
        """
        Converts a result given in a vector format where each index corresponds
        to DOF of the system into a dictionnary that stores data using node name
        and direction
        """
        res2 = collections.OrderedDict()
        
        if type == "disp":
            ldnom = ['DX','DY','DZ','RX','RY','RZ']
        elif type == "force":
            ldnom = ['FX','FY','FZ','MX','MY','MZ']
        else:
            print("Error (assembly.dof2node): no valid type of data is given!")
        
        for no in model.DOF['NODE'].keys():
            res2[no] = collections.OrderedDict()
            for i,nom in zip(model.DOF['NODE'][no],ldnom):
                res2[no][nom] = res[i]
        
        return res2
    
    
    @staticmethod
    def node2dof(model,res,type):
        """
        Converts a result given in a dictionnary format into an array format
        """
        
        if type == "disp":
            ldnom = ['DX','DY','DZ','RX','RY','RZ']
        elif type == "force":
            ldnom = ['FX','FY','FZ','MX','MY','MZ']
        else:
            print("Error (assembly.dof2node): no valid type of data is given!")
        
        aux = []
        for no in model.DOF['NODE'].keys():
            for dir in ldnom:
                aux.append(res[no][dir])
        
        return numpy.array(aux)
    
    
    @staticmethod
    def get_K(model):
        """
        Assemblage de la matrice de raideur du système
        """
        
        # degrés de liberté dans le problème
        DOF  = model.DOF
        n_dof = len(DOF["NODE"].keys())*6
        
        # initialisation de la matrice de raideur du système
        K = numpy.zeros((n_dof,n_dof), dtype=numpy.float)
        
        # Assemblage de la matrice de raideur
        for elem in model.ELEM:
            if isinstance(elem,CATA_ELEM_STIFF):
                nodes = elem.nodes
                ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
                K_elem = elem.K
                K = assembly.matrix_assembly(K, K_elem, ldof)
        
        return K
    
    
    @staticmethod
    def get_M(model):
        """
        Assemblage de la matrice de masse du système
        """
        
        # degrés de liberté dans le problème
        DOF  = model.DOF
        n_dof = len(DOF["NODE"].keys())*6
        
        # initialisation de la matrice de masse du système
        M = numpy.zeros((n_dof,n_dof), dtype=numpy.float)
        
        # Assemblage de la matrice de masse
        for elem in model.ELEM:
            if isinstance(elem,CATA_ELEM_MASS):
                nodes = elem.nodes
                ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
                M_elem = elem.M
                M = assembly.matrix_assembly(M, M_elem, ldof)
        
        return M
    
    
    @staticmethod
    def get_C(model):
        """
        Assemblage de la matrice d'amortissement du système
        
        IMPORTANTE: lorsque la matrice d'amortissement de Rayleigh
        est calculée, il faut faire son calcul après l'assemblage
        de la matrice de raideur et de masse du système
        """
        
        # degrés de liberté dans le problème
        DOF  = model.DOF
        n_dof = len(DOF["NODE"].keys())*6
        
        # initialisation de la matrice d'amortissement du système
        C = numpy.zeros((n_dof,n_dof), dtype=numpy.float)
        
        # Assemblage de la matrice d'amortissement
        for elem in model.ELEM:
            if isinstance(elem,CATA_ELEM_DAMP):
                nodes = elem.nodes
                ldof = numpy.concatenate([DOF['NODE'][no] for no in nodes],axis=0)
                C_elem = elem.C
                C = assembly.matrix_assembly(C, C_elem, ldof)
        
        # Calcul de la matrice d'amortissement global de Rayleigh
        if model.DAMPING:
            if hasattr(model,"M") and hasattr(model,"K"):
                if model.DAMPING["type"] == "RAYLEIGH":
                    w1  = 2.*pi*model.DAMPING["f1"]
                    w2  = 2.*pi*model.DAMPING["f2"]
                    xi1 = model.DAMPING["xi1"]
                    xi2 = model.DAMPING["xi2"]
                    a_ = numpy.array([[w2,-w1],[-1./w2,1./w1]])
                    b_ = numpy.array([xi1,xi2])
                    a0,a1 = (2.*(w1*w2)/(w2**2-w1**2))*dot(a_,b_)
                    C += a0*M + a1*K
            else:
                pmsg("Error (assembly.get_C): global stiffness and/or mass matrix" +\
                     "are not available in the model!")
        
        return C
    
